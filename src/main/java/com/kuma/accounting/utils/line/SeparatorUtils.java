package com.kuma.accounting.utils.line;

import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.component.Separator.SeparatorBuilder;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SeparatorUtils {

	public static Separator newSeparator() {
		return getSeparatorBuilder().build();
	}

	private static SeparatorBuilder getSeparatorBuilder() {
		return Separator.builder();
	}
}
