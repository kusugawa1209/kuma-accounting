package com.kuma.accounting.utils.line;

import static com.kuma.accounting.message.enums.TextTemplate.ACCOUNT_CURRENT_AMOUNT_FORMAT;
import static com.kuma.accounting.message.enums.TextTemplate.ACCOUNT_NAME_FORMAT;
import static com.kuma.accounting.message.enums.TextTemplate.BILLING_DATE_FORMAT;
import static com.kuma.accounting.message.enums.TextTemplate.CREDIT_CARD_CURRENT_AMOUNT_FORMAT;
import static com.kuma.accounting.message.enums.TextTemplate.CREDIT_CARD_NAME_FORMAT;
import static com.kuma.accounting.message.enums.TextTemplate.DUE_DATE_FORMAT;
import static com.kuma.accounting.message.enums.TextTemplate.QUOTA_FORMAT;
import static com.kuma.accounting.utils.line.ButtonUtils.getCancelButton;
import static com.kuma.accounting.utils.line.ButtonUtils.getConfirmButton;
import static com.kuma.accounting.utils.line.ButtonUtils.getMenuButton;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;

import com.google.common.collect.Lists;
import com.kuma.accounting.dto.AbstractStatistics;
import com.kuma.accounting.dto.DayStatistics;
import com.kuma.accounting.dto.MonthStatistics;
import com.kuma.accounting.dto.NormalRecordStatistics;
import com.kuma.accounting.dto.TransferRecordStatistics;
import com.kuma.accounting.info.AccountInfo;
import com.kuma.accounting.info.BaseAccountInfo;
import com.kuma.accounting.info.CreditCardInfo;
import com.kuma.accounting.info.NormalRecordInfo;
import com.kuma.accounting.info.TransferRecordInfo;
import com.kuma.accounting.message.enums.AccountType;
import com.kuma.accounting.message.enums.Format;
import com.kuma.accounting.message.enums.Message;
import com.kuma.accounting.message.enums.RecordType;
import com.kuma.accounting.message.enums.TextTemplate;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Box.BoxBuilder;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexPaddingSize;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BoxUtils {

	private static final String HEADER_BACKGROUND_COLOR = "#3379B7";

	private static final int BUTTONS_PER_ROW = 2;

	public static Box newMonthStatisticBody(MonthStatistics monthStatistics) {
		List<FlexComponent> statisticsInfos = Lists.newArrayList();

		appendMonthAmountText(statisticsInfos, monthStatistics);

		appendMonthIncomeOutGoingDifferenceText(statisticsInfos, monthStatistics);

		appendEmptyRecordsInfo(statisticsInfos);

		return BoxUtils.newBody(statisticsInfos);
	}

	public static Box newDayStatisticBody(DayStatistics dayStatistics) {
		List<FlexComponent> statisticsInfos = Lists.newArrayList();

		appendDayAmountText(statisticsInfos, dayStatistics);

		appendDayIncomeOutGoingDifferenceText(statisticsInfos, dayStatistics);

		appendRecordTexts(dayStatistics, statisticsInfos);

		appendTransferRecordTexts(dayStatistics, statisticsInfos);

		appendEmptyRecordsInfo(statisticsInfos);

		return BoxUtils.newBody(statisticsInfos);
	}

	public static Box newAccountInfoBody(AccountInfo accountInfo) {
		List<FlexComponent> accountInfos = Lists.newArrayList();
		AccountType accountType = accountInfo.getType();

		TextTemplate accountNameTemplate;
		TextTemplate amountTemplate;
		if (AccountType.ACCOUNT.equals(accountType)) {
			accountNameTemplate = ACCOUNT_NAME_FORMAT;
			amountTemplate = ACCOUNT_CURRENT_AMOUNT_FORMAT;
		} else {
			accountNameTemplate = CREDIT_CARD_NAME_FORMAT;
			amountTemplate = CREDIT_CARD_CURRENT_AMOUNT_FORMAT;
		}

		String accountNameDescription = accountNameTemplate.format(accountInfo.getName());
		Text accountNameText = TextUtils.newBodyText(accountNameDescription);
		accountInfos.add(accountNameText);

		NumberFormat formatter = new DecimalFormat(Format.AMOUNT_FORMAT.getStringFormat());
		String formatedInitialAmount = formatter.format(accountInfo.getCurrentAmount());

		String initialAmountDescription = amountTemplate.format(formatedInitialAmount);
		Text initialAmountText = TextUtils.newBodyText(initialAmountDescription);
		accountInfos.add(initialAmountText);

		return BoxUtils.newBody(accountInfos);
	}

	public static Box newTransferAccountInfoBody(AccountInfo sourceAccountInfo, AccountInfo targetAccountInfo) {
		Box sourceAccountInfoBox = newAccountInfoBody(sourceAccountInfo);
		Box targetAccountInfoBox = newAccountInfoBody(targetAccountInfo);

		return BoxUtils.newBody(sourceAccountInfoBox, targetAccountInfoBox);

	}

	public static Box newNormalRecordInfoBody(NormalRecordInfo recordInfo) {
		List<String> recordInfos = Lists.newArrayList();

		RecordType recordType = recordInfo.getType();
		String recordTypeText = recordType.getText();

		Date date = recordInfo.getDate();
		String dateInfo = getDateInfo(recordType, date);
		recordInfos.add(dateInfo);

		BaseAccountInfo account = recordInfo.getAccount();
		String accountName = account.getName();
		String accountType = account.getType().getName();

		String accountTypeInfo = TextTemplate.RECORD_ACCOUNT_TYPE_FORMAT.format(recordTypeText, accountType);
		recordInfos.add(accountTypeInfo);

		String accountNameInfo = TextTemplate.RECORD_ACCOUNT_NAME_FORMAT.format(recordTypeText, accountType,
				accountName);
		recordInfos.add(accountNameInfo);

		String description = recordInfo.getDescription();
		String itemInfo = getItemInfo(recordType, description);
		recordInfos.add(itemInfo);

		BigDecimal amount = recordInfo.getAmount();
		String amountInfo = getAmountInfo(recordType, amount);
		recordInfos.add(amountInfo);

		return BoxUtils.newBodyByTexts(recordInfos);
	}

	public static Box newTransferRecordInfoBody(TransferRecordInfo transferRecordInfo) {
		List<String> recordInfos = Lists.newArrayList();

		RecordType recordType = RecordType.TRANSFER;

		Date date = transferRecordInfo.getDate();
		String dateInfo = getDateInfo(recordType, date);
		recordInfos.add(dateInfo);

		BaseAccountInfo sourceAccount = transferRecordInfo.getSourceAccount();
		String sourceAccountType = sourceAccount.getType().getName();
		String sourceAccountName = sourceAccount.getName();

		String sourceAccountTypeInfo = TextTemplate.RECORD_SOURCE_ACCOUNT_TYPE_FORMAT.format(sourceAccountType);
		recordInfos.add(sourceAccountTypeInfo);

		String sourceAccountNameInfo = TextTemplate.RECORD_SOURCE_ACCOUNT_NAME_FORMAT.format(sourceAccountType,
				sourceAccountName);
		recordInfos.add(sourceAccountNameInfo);

		BaseAccountInfo targetAccount = transferRecordInfo.getTargetAccount();
		String targetAccountType = targetAccount.getType().getName();
		String targetAccountName = targetAccount.getName();

		String targetAccountTypeInfo = TextTemplate.RECORD_TARGET_ACCOUNT_TYPE_FORMAT.format(targetAccountType);
		recordInfos.add(targetAccountTypeInfo);

		String targetAccountNameInfo = TextTemplate.RECORD_TARGET_ACCOUNT_NAME_FORMAT.format(targetAccountType,
				targetAccountName);
		recordInfos.add(targetAccountNameInfo);

		String description = transferRecordInfo.getDescription();
		String itemInfo = getItemInfo(recordType, description);
		recordInfos.add(itemInfo);

		BigDecimal amount = transferRecordInfo.getAmount();
		String amountInfo = getAmountInfo(recordType, amount);
		recordInfos.add(amountInfo);

		return BoxUtils.newBodyByTexts(recordInfos);
	}

	public static Box newFunctionMenu() {
		List<FlexComponent> functionButtons = Lists.newArrayList();
		functionButtons.addAll(ButtonUtils.getFunctionButtons());

		List<FlexComponent> components = Lists.newArrayList();
		List<Box> buttonRows = Lists.partition(functionButtons, BUTTONS_PER_ROW).stream()
				.map(buttons -> BoxUtils.newBox(buttons, FlexLayout.HORIZONTAL)).collect(Collectors.toList());

		components.addAll(buttonRows);

		return getVerticalBoxBuilder().contents(components).build();
	}

	public static Box newRecordTypeButtonsBody() {
		List<FlexComponent> recordTypeButtons = Lists.newArrayList();
		recordTypeButtons.addAll(ButtonUtils.getRecordTypeButtons());

		List<FlexComponent> components = Lists.newArrayList();
		List<Box> buttonRows = Lists.partition(recordTypeButtons, BUTTONS_PER_ROW).stream()
				.map(buttons -> BoxUtils.newBox(buttons, FlexLayout.HORIZONTAL)).collect(Collectors.toList());

		components.addAll(buttonRows);

		return getVerticalBoxBuilder().contents(components).build();
	}

	public static Box newCreditCardInfoBody(CreditCardInfo creditCardInfo) {
		List<FlexComponent> creditCardInfos = Lists.newArrayList();
		String creditCardNameDescription = CREDIT_CARD_NAME_FORMAT.format(creditCardInfo.getName());
		Text creditCardNameText = TextUtils.newBodyText(creditCardNameDescription);
		creditCardInfos.add(creditCardNameText);

		NumberFormat formatter = new DecimalFormat(Format.AMOUNT_FORMAT.getStringFormat());
		String formatedQuota = formatter.format(creditCardInfo.getQuota());
		String quotaDescription = QUOTA_FORMAT.format(formatedQuota);
		Text quotaText = TextUtils.newBodyText(quotaDescription);
		creditCardInfos.add(quotaText);

		String dueDateDescription = DUE_DATE_FORMAT.format(creditCardInfo.getDueDate());
		Text dueDateText = TextUtils.newBodyText(dueDateDescription);
		creditCardInfos.add(dueDateText);

		String billingDateDescription = BILLING_DATE_FORMAT.format(creditCardInfo.getBillingDate());
		Text billingDateText = TextUtils.newBodyText(billingDateDescription);
		creditCardInfos.add(billingDateText);

		String formatedInitialAmount = formatter.format(creditCardInfo.getCurrentAmount());
		String initialAmountDescription = CREDIT_CARD_CURRENT_AMOUNT_FORMAT.format(formatedInitialAmount);
		Text initialAmountText = TextUtils.newBodyText(initialAmountDescription);
		creditCardInfos.add(initialAmountText);

		return BoxUtils.newBody(creditCardInfos);
	}

	public static Box newHeader(String text) {
		Text headerText = TextUtils.newHeaderText(text);
		return getVerticalBoxBuilder().backgroundColor(HEADER_BACKGROUND_COLOR).content(headerText)
				.paddingAll(FlexPaddingSize.LG).build();
	}

	public static Box newBox(List<FlexComponent> components, FlexLayout layout) {
		return getBoxBuilder(layout).contents(components).build();
	}

	public static Box newBody(String text) {
		Text bodyText = TextUtils.newBodyText(text);
		return getVerticalBoxBuilder().content(bodyText).build();
	}

	public static Box newBody(FlexComponent... components) {
		return getVerticalBoxBuilder().contents(components).build();
	}

	public static Box newBody(List<FlexComponent> components) {
		return getVerticalBoxBuilder().contents(components).build();
	}

	public static Box newBodyByTexts(List<String> texts) {
		List<FlexComponent> textComponents = texts.stream().map(TextUtils::newBodyText).collect(Collectors.toList());

		return getVerticalBoxBuilder().contents(textComponents).build();
	}

	public static Box newCancelFooter() {
		return getHorizontalBoxBuilder().contents(getCancelButton()).build();
	}

	public static Box newConfirmAndCancelFooter() {
		return getHorizontalBoxBuilder().contents(getConfirmButton(), getCancelButton()).build();
	}

	public static Box newMenuFooter() {
		return getHorizontalBoxBuilder().contents(getMenuButton()).build();
	}

	public static Box newFooter(FlexComponent... components) {
		return getHorizontalBoxBuilder().contents(components).build();
	}

	private static String getDateInfo(RecordType recordType, Date date) {
		String recordTypeText = recordType.getText();

		DateFormat format = new SimpleDateFormat(Format.STATISTICS_DATE_FORMAT.getStringFormat());
		String dateText = format.format(date);

		return TextTemplate.RECORD_DATE_FORMAT.format(recordTypeText, dateText);
	}

	private static String getItemInfo(RecordType recordType, String description) {
		String recordTypeText = recordType.getText();
		return TextTemplate.RECORD_ITEM_FORMAT.format(recordTypeText, description);
	}

	private static String getAmountInfo(RecordType recordType, BigDecimal amount) {
		String recordTypeText = recordType.getText();

		NumberFormat formatter = new DecimalFormat(Format.AMOUNT_FORMAT.getStringFormat());
		String amountText = formatter.format(amount);

		return TextTemplate.RECORD_AMOUNT_FORMAT.format(recordTypeText, amountText);
	}

	private static void appendEmptyRecordsInfo(List<FlexComponent> statisticsInfos) {
		if (CollectionUtils.isEmpty(statisticsInfos)) {
			String emptyRecordsText = Message.EMPTY_RECORDS.getText();
			Text text = TextUtils.newBodyText(emptyRecordsText);

			statisticsInfos.add(text);
		}
	}

	private static void appendMonthAmountText(List<FlexComponent> statisticsInfos, MonthStatistics monthStatistics) {
		appendAmountText(statisticsInfos, monthStatistics, TextTemplate.MONTH_RECORD_TYPE);
	}

	private static void appendDayAmountText(List<FlexComponent> statisticsInfos, DayStatistics dayStatistics) {
		appendAmountText(statisticsInfos, dayStatistics, TextTemplate.DAY_RECORD_TYPE);
	}

	private static void appendMonthIncomeOutGoingDifferenceText(List<FlexComponent> statisticsInfos,
			MonthStatistics monthStatistics) {
		appendIncomeOutGoingDifferenceText(statisticsInfos, monthStatistics, TextTemplate.MONTH_INCOME_MINUS_OUT_GOING);
	}

	private static void appendDayIncomeOutGoingDifferenceText(List<FlexComponent> statisticsInfos,
			DayStatistics dayStatistics) {
		appendIncomeOutGoingDifferenceText(statisticsInfos, dayStatistics, TextTemplate.DAY_INCOME_MINUS_OUT_GOING);
	}

	private static <S extends AbstractStatistics> void appendIncomeOutGoingDifferenceText(
			List<FlexComponent> statisticsInfos, S statistics, TextTemplate textTemplate) {
		Map<RecordType, BigDecimal> amounts = statistics.getAmounts();
		BigDecimal incomeAmount = amounts.get(RecordType.INCOME);
		BigDecimal outGoingAmount = amounts.get(RecordType.OUT_GOING);

		if (incomeAmount.signum() > 0 || outGoingAmount.signum() > 0) {
			NumberFormat formatter = new DecimalFormat(Format.AMOUNT_FORMAT.getStringFormat());
			BigDecimal differenceAmount = incomeAmount.subtract(outGoingAmount);
			String formatedAmount = formatter.format(differenceAmount);

			String amountDescription = textTemplate.format(formatedAmount);
			Text text = TextUtils.newBodyText(amountDescription);

			statisticsInfos.add(text);
		}
	}

	private static <S extends AbstractStatistics> void appendAmountText(List<FlexComponent> statisticsInfos,
			S statistics, TextTemplate textTemplate) {
		NumberFormat formatter = new DecimalFormat(Format.AMOUNT_FORMAT.getStringFormat());

		Map<RecordType, BigDecimal> amounts = statistics.getAmounts();
		amounts.entrySet().stream().filter(amount -> amount.getValue().signum() > 0)
				.sorted((entry1, entry2) -> entry1.getKey().compareTo(entry2.getKey())).map(amount -> {
					String recordTypeText = amount.getKey().getText();

					String formatedAmount = formatter.format(amount.getValue());

					String amountDescription = textTemplate.format(recordTypeText, formatedAmount);
					return TextUtils.newBodyText(amountDescription);
				}).forEach(statisticsInfos::add);
	}

	private static void appendTransferRecordTexts(DayStatistics dayStatistics, List<FlexComponent> statisticsInfos) {
		List<TransferRecordStatistics> transferRecords = dayStatistics.getTransferRecords();
		if (CollectionUtils.isNotEmpty(transferRecords)) {
			appendSeperator(statisticsInfos);

			NumberFormat formatter = new DecimalFormat(Format.AMOUNT_FORMAT.getStringFormat());

			Text recordTypeText = TextUtils.newBodyText(RecordType.TRANSFER.getText());
			statisticsInfos.add(recordTypeText);

			List<Text> recordTexts = transferRecords.stream().map(record -> {
				String sourceAccountName = record.getSourceAccountName();
				String targetAccountName = record.getTargetAccountName();

				String description = record.getDescription();

				BigDecimal amount = record.getAmount();
				String amountText = formatter.format(amount);

				String recordText = TextTemplate.TRANSFER_RECORD.format(sourceAccountName, targetAccountName,
						description, amountText);

				return TextUtils.newBodyText(recordText);
			}).collect(Collectors.toList());

			statisticsInfos.addAll(recordTexts);
		}
	}

	private static void appendRecordTexts(DayStatistics dayStatistics, List<FlexComponent> statisticsInfos) {
		List<NormalRecordStatistics> normalRecords = dayStatistics.getNormalRecords();
		Map<RecordType, List<NormalRecordStatistics>> groupedNormalRecords = normalRecords.stream()
				.collect(Collectors.groupingBy(NormalRecordStatistics::getRecordType));

		groupedNormalRecords.entrySet().stream().filter(entry -> CollectionUtils.isNotEmpty(entry.getValue()))
				.sorted((entry1, entry2) -> entry1.getKey().compareTo(entry2.getKey())).forEach(entry -> {
					appendSeperator(statisticsInfos);

					RecordType recordType = entry.getKey();

					List<NormalRecordStatistics> records = entry.getValue();
					Collections.sort(records);

					appendRecordTexts(statisticsInfos, records, recordType);
				});
	}

	private static void appendRecordTexts(List<FlexComponent> statisticsInfos, List<NormalRecordStatistics> records,
			RecordType recordType) {
		NumberFormat formatter = new DecimalFormat(Format.AMOUNT_FORMAT.getStringFormat());

		Text recordTypeText = TextUtils.newBodyText(recordType.getText());
		statisticsInfos.add(recordTypeText);

		List<Text> recordTexts = records.stream().map(record -> {
			String accountName = record.getAccountName();
			String description = record.getDescription();

			BigDecimal amount = record.getAmount();
			String amountText = formatter.format(amount);

			String recordText = TextTemplate.RECORD.format(accountName, description, amountText);

			return TextUtils.newBodyText(recordText);
		}).collect(Collectors.toList());

		statisticsInfos.addAll(recordTexts);
	}

	private static void appendSeperator(List<FlexComponent> statisticsInfos) {
		Separator separator = SeparatorUtils.newSeparator();
		statisticsInfos.add(separator);
	}

	private static BoxBuilder getBoxBuilder(FlexLayout layout) {
		return getBaseBoxBuilder().layout(layout);
	}

	private static BoxBuilder getVerticalBoxBuilder() {
		return getBoxBuilder(FlexLayout.VERTICAL);
	}

	private static BoxBuilder getHorizontalBoxBuilder() {
		return getBoxBuilder(FlexLayout.HORIZONTAL);
	}

	private static BoxBuilder getBaseBoxBuilder() {
		return Box.builder().paddingAll(FlexPaddingSize.MD);
	}
}
