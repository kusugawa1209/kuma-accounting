package com.kuma.accounting.utils.line;

import com.linecorp.bot.model.message.flex.component.Span;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextBuilder;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TextUtils {

	private static final String HEADER_TEXT_COLOR = "#FFFFFF";

	public static Text newHeaderText(String text) {
		Span span = Span.builder().text(text).color(HEADER_TEXT_COLOR).build();

		return getBaseTextBuilder().content(span).build();
	}

	public static Text newBodyText(String text) {
		Span span = Span.builder().text(text).build();

		return getBaseTextBuilder().content(span).build();
	}

	private static TextBuilder getBaseTextBuilder() {
		return Text.builder().wrap(true);
	}
}
