package com.kuma.accounting.utils.line;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.kuma.accounting.dto.DayStatistics;
import com.kuma.accounting.dto.MonthStatistics;
import com.kuma.accounting.info.AccountInfo;
import com.kuma.accounting.info.NormalRecordInfo;
import com.kuma.accounting.message.enums.Format;
import com.kuma.accounting.message.enums.Message;
import com.kuma.accounting.message.enums.TextTemplate;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.Bubble.BubbleBuilder;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BubbleUtils {

	public static Bubble newMonthStatisticsBubble(MonthStatistics monthStatistics) {
		String monthText = TextTemplate.MONTH_STATISTICS.format(monthStatistics.getYear(), monthStatistics.getMonth());
		Box header = BoxUtils.newHeader(monthText);
		Box body = BoxUtils.newMonthStatisticBody(monthStatistics);

		Box footer = BoxUtils.newMenuFooter();

		return newBubble(header, body, footer);
	}

	public static Bubble newDayStatisticsBubble(DayStatistics dayStatistics) {
		DateFormat dateFormat = new SimpleDateFormat(Format.STATISTICS_DATE_FORMAT.getStringFormat());
		String dayText = dateFormat.format(dayStatistics.getDate());
		Box header = BoxUtils.newHeader(TextTemplate.DAY_STATISTICS.format(dayText));
		Box body = BoxUtils.newDayStatisticBody(dayStatistics);

		Box footer = BoxUtils.newMenuFooter();

		return newBubble(header, body, footer);
	}

	public static Bubble newAccountInfoBubble(AccountInfo accountInfo) {
		String headerText = TextTemplate.BASE_ACCOUNT_NAME_INFO_FORMAT.format(accountInfo.getName());
		Box header = BoxUtils.newHeader(headerText);

		Box body = BoxUtils.newAccountInfoBody(accountInfo);

		Box footer = BoxUtils.newMenuFooter();

		return newBubble(header, body, footer);
	}

	public static Bubble newTransferAccountInfoBubble(AccountInfo sourceAccountInfo, AccountInfo targetAccountInfo) {
		String headerText = Message.TRANSFER_INFO.getText();
		Box header = BoxUtils.newHeader(headerText);

		Box body = BoxUtils.newTransferAccountInfoBody(sourceAccountInfo, targetAccountInfo);

		Box footer = BoxUtils.newMenuFooter();

		return newBubble(header, body, footer);
	}

	public static Bubble newRecordInfoBubble(Box header, NormalRecordInfo recordInfo, AccountInfo accountInfo,
			Box footer) {
		Box recordInfoBox = BoxUtils.newNormalRecordInfoBody(recordInfo);

		Separator separator = SeparatorUtils.newSeparator();

		Box accountInfoBox = BoxUtils.newAccountInfoBody(accountInfo);

		Box body = BoxUtils.newBody(recordInfoBox, separator, accountInfoBox);

		return newBubble(header, body, footer);
	}

	public static Bubble newBubble(Box body) {
		return getBaseBubbleBuilder().body(body).build();
	}

	public static Bubble newBubble(Box header, Box body) {
		return getBaseBubbleBuilder().header(header).body(body).build();
	}

	public static Bubble newBubble(Box header, Box body, Box footer) {
		return getBaseBubbleBuilder().header(header).body(body).footer(footer).build();
	}

	private static BubbleBuilder getBaseBubbleBuilder() {
		return Bubble.builder();
	}

}
