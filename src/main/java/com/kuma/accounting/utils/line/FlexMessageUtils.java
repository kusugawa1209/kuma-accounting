package com.kuma.accounting.utils.line;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.FlexMessage.FlexMessageBuilder;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.Carousel;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FlexMessageUtils {

	public static FlexMessage newMessage(String altText, Box header, Box body, Box footer) {
		Bubble container = BubbleUtils.newBubble(header, body, footer);

		return getBaseFlexMessageBuilder(altText).contents(container).build();
	}

	public static FlexMessage newCarouselMessage(String altText, List<Bubble> bubbles) {
		Carousel carousel = Carousel.builder().contents(bubbles).build();

		return getBaseFlexMessageBuilder(altText).contents(carousel).build();
	}

	public static FlexMessage newCarouselMessage(String altText, Bubble... bubbles) {
		List<Bubble> bubblesList = Arrays.stream(bubbles).collect(Collectors.toList());
		Carousel carousel = Carousel.builder().contents(bubblesList).build();

		return getBaseFlexMessageBuilder(altText).contents(carousel).build();
	}

	public static FlexMessage newMessageWithCancelFooter(String headerText, Box body) {
		Box header = BoxUtils.newHeader(headerText);
		Box footer = BoxUtils.newCancelFooter();

		Bubble container = BubbleUtils.newBubble(header, body, footer);

		return getBaseFlexMessageBuilder(headerText).contents(container).build();
	}

	public static FlexMessage newMessageWithCancelFooter(String headerText, String bodyText) {
		Box header = BoxUtils.newHeader(headerText);
		Box body = BoxUtils.newBody(bodyText);
		Box footer = BoxUtils.newCancelFooter();

		Bubble container = BubbleUtils.newBubble(header, body, footer);

		return getBaseFlexMessageBuilder(headerText).contents(container).build();
	}

	public static FlexMessage newMessage(String headerText, FlexComponent... components) {
		Box header = BoxUtils.newHeader(headerText);
		Box body = BoxUtils.newBody(components);

		Bubble container = BubbleUtils.newBubble(header, body);

		return getBaseFlexMessageBuilder(headerText).contents(container).build();
	}

	public static FlexMessage newMessage(String headerText, List<FlexComponent> components) {
		Box header = BoxUtils.newHeader(headerText);
		Box body = BoxUtils.newBody(components);

		Bubble container = BubbleUtils.newBubble(header, body);

		return getBaseFlexMessageBuilder(headerText).contents(container).build();
	}

	public static FlexMessage newMessage(String headerText, Box body) {
		Box header = BoxUtils.newHeader(headerText);

		Bubble container = BubbleUtils.newBubble(header, body);

		return getBaseFlexMessageBuilder(headerText).contents(container).build();
	}

	public static FlexMessage newConfirmAndCancelMessage(String headerText, FlexComponent... components) {
		Box header = BoxUtils.newHeader(headerText);
		Box body = BoxUtils.newBody(components);
		Box footer = BoxUtils.newConfirmAndCancelFooter();

		Bubble container = BubbleUtils.newBubble(header, body, footer);

		return getBaseFlexMessageBuilder(headerText).contents(container).build();
	}

	public static FlexMessage newConfirmAndCancelMessage(String headerText, List<FlexComponent> components) {
		Box header = BoxUtils.newHeader(headerText);
		Box body = BoxUtils.newBody(components);
		Box footer = BoxUtils.newConfirmAndCancelFooter();

		Bubble container = BubbleUtils.newBubble(header, body, footer);

		return getBaseFlexMessageBuilder(headerText).contents(container).build();
	}

	private static FlexMessageBuilder getBaseFlexMessageBuilder(String altText) {
		return FlexMessage.builder().altText(altText);
	}
}
