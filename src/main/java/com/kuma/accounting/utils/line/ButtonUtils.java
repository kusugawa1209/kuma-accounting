package com.kuma.accounting.utils.line;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.kuma.accounting.message.enums.Process;
import com.kuma.accounting.message.enums.RecordType;
import com.linecorp.bot.model.action.MessageAction;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Button.ButtonBuilder;
import com.linecorp.bot.model.message.flex.component.Button.ButtonHeight;
import com.linecorp.bot.model.message.flex.component.Button.ButtonStyle;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ButtonUtils {

	private static final String BUTTON_COLOR = "#286090";

	private static final String CONFIRM = "確認";

	private static final String CANCEL = "取消";

	public static Button newButton(String label, String text) {
		return getBaseButtonBuilder().action(new MessageAction(label, text)).build();
	}

	public static Button newButton(String text) {
		return getBaseButtonBuilder().action(new MessageAction(text, text)).build();
	}

	public static Button getCancelButton() {
		return newButton(CANCEL);
	}

	public static Button getConfirmButton() {
		return newButton(CONFIRM);
	}

	public static Button getMenuButton() {
		return newButton(Process.SHOW_FUNCTION_MENU.getText());
	}

	public static List<Button> getFunctionButtons() {
		return Process.getFunctionProcesses().stream().map(process -> {
			String processText = process.getText();

			return newButton(processText);
		}).collect(Collectors.toList());
	}

	public static List<Button> getRecordTypeButtons() {
		return Arrays.stream(RecordType.values()).map(recordType -> {
			String recordTypeText = recordType.getText();

			return newButton(recordTypeText);
		}).collect(Collectors.toList());
	}

	private static ButtonBuilder getBaseButtonBuilder() {
		return Button.builder().height(ButtonHeight.SMALL).style(ButtonStyle.PRIMARY).margin(FlexMarginSize.MD)
				.color(BUTTON_COLOR);
	}

}
