package com.kuma.accounting.dto;

import com.kuma.accounting.info.BaseAccountInfo;
import com.kuma.accounting.info.TransferRecordInfo;
import com.kuma.accounting.message.enums.AccountType;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true, of = { "targetAccountType", "targetAccountName" })
public class TransferRecordParameter extends NormalRecordParameter implements TransferRecordInfo {

	private AccountType targetAccountType = null;

	private String targetAccountName = null;

	@Override
	public BaseAccountInfo getSourceAccount() {
		return new BaseAccountInfo() {

			@Override
			public AccountType getType() {
				return getAccountType();
			}

			@Override
			public String getName() {
				return getAccountName();
			}
		};
	}

	@Override
	public BaseAccountInfo getTargetAccount() {
		return new BaseAccountInfo() {

			@Override
			public AccountType getType() {
				return targetAccountType;
			}

			@Override
			public String getName() {
				return targetAccountName;
			}
		};
	}

}
