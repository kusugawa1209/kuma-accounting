package com.kuma.accounting.dto;

import java.math.BigDecimal;

import com.kuma.accounting.info.AccountInfo;
import com.kuma.accounting.message.enums.AccountType;

import lombok.Data;

@Data
public class AccountParameter implements ProcessParameter, AccountInfo {

	private String accountName = null;

	private BigDecimal initialAmount = null;

	@Override
	public AccountType getType() {
		return AccountType.ACCOUNT;
	}

	@Override
	public String getName() {
		return this.accountName;
	}

	@Override
	public BigDecimal getCurrentAmount() {
		return this.initialAmount;
	}

}
