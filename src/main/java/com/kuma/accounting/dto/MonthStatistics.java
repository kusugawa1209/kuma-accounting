package com.kuma.accounting.dto;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import com.kuma.accounting.model.NormalRecord;

import lombok.Getter;

@Getter
public class MonthStatistics extends AbstractStatistics {

	private Integer year;

	private Integer month;

	public MonthStatistics(Date date, Collection<NormalRecord> records) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		this.year = calendar.get(Calendar.YEAR);
		this.month = calendar.get(Calendar.MONTH) + 1;

		this.amounts = sumNormalRecords(records);
	}

}
