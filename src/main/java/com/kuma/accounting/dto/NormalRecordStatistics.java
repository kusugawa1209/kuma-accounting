package com.kuma.accounting.dto;

import java.math.BigDecimal;
import java.util.Comparator;

import com.kuma.accounting.message.enums.AccountType;
import com.kuma.accounting.message.enums.RecordType;
import com.kuma.accounting.model.BaseAccount;
import com.kuma.accounting.model.NormalRecord;

import lombok.Value;

@Value
public class NormalRecordStatistics implements Comparable<NormalRecordStatistics> {

	private RecordType recordType;

	private AccountType accountType;

	private String accountName;

	private String description;

	private BigDecimal amount;

	public NormalRecordStatistics(NormalRecord normalRecord) {
		this.recordType = normalRecord.getType();

		BaseAccount account = normalRecord.getAccount();
		this.accountType = account.getType();
		this.accountName = account.getName();

		this.description = normalRecord.getDescription();
		this.amount = normalRecord.getAmount();
	}

	@Override
	public int compareTo(NormalRecordStatistics o) {
		return Comparator.comparing(NormalRecordStatistics::getRecordType)
				.thenComparing(NormalRecordStatistics::getAccountType)
				.thenComparing(NormalRecordStatistics::getAccountName)
				.thenComparing(NormalRecordStatistics::getDescription).thenComparing(NormalRecordStatistics::getAmount)
				.compare(this, o);
	}
}
