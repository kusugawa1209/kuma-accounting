package com.kuma.accounting.dto;

import com.kuma.accounting.info.BaseAccountInfo;
import com.kuma.accounting.info.NormalRecordInfo;
import com.kuma.accounting.message.enums.AccountType;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true, of = { "accountType", "accountName" })
public class NormalRecordParameter extends BaseRecordParameter implements NormalRecordInfo {

	private AccountType accountType = null;

	private String accountName = null;

	@Override
	public BaseAccountInfo getAccount() {
		return new BaseAccountInfo() {

			@Override
			public AccountType getType() {
				return accountType;
			}

			@Override
			public String getName() {
				return accountName;
			}

		};
	}

}
