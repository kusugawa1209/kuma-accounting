package com.kuma.accounting.dto;

import java.math.BigDecimal;

import com.kuma.accounting.info.CreditCardInfo;
import com.kuma.accounting.message.enums.AccountType;

import lombok.Data;

@Data
public class CreditCardParameter implements ProcessParameter, CreditCardInfo {

	private String creditCardName = null;

	private BigDecimal quota = null;

	private Integer dueDate = null;

	private Integer billingDate = null;

	private BigDecimal initialAmount = null;

	@Override
	public AccountType getType() {
		return AccountType.CREDIT_CARD;
	}

	@Override
	public String getName() {
		return this.creditCardName;
	}

	@Override
	public BigDecimal getCurrentAmount() {
		return this.initialAmount;
	}

}
