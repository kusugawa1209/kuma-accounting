package com.kuma.accounting.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.kuma.accounting.message.enums.RecordType;

import lombok.Data;

@Data
public class BaseRecordParameter implements ProcessParameter {

	protected RecordType type = null;

	protected Date date = null;

	protected String description = null;

	protected BigDecimal amount = null;

}
