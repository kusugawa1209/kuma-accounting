package com.kuma.accounting.dto;

import java.util.Date;
import java.util.List;

import com.kuma.accounting.model.NormalRecord;
import com.kuma.accounting.model.TransferRecord;
import com.kuma.utils.CollectionUtils2;

import lombok.Getter;

@Getter
public class DayStatistics extends AbstractStatistics {

	private Date date;

	private List<NormalRecordStatistics> normalRecords;

	private List<TransferRecordStatistics> transferRecords;

	public DayStatistics(Date date, List<NormalRecord> normalRecords, List<TransferRecord> transferRecords) {
		this.date = date;

		this.normalRecords = CollectionUtils2.mappingToList(normalRecords, NormalRecordStatistics::new);
		this.transferRecords = CollectionUtils2.mappingToList(transferRecords, TransferRecordStatistics::new);

		this.amounts = sumNormalRecords(normalRecords);
	}
}
