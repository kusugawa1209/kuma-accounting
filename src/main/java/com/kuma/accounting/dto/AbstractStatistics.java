package com.kuma.accounting.dto;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;

import com.kuma.accounting.message.enums.RecordHandlingType;
import com.kuma.accounting.message.enums.RecordType;
import com.kuma.accounting.model.NormalRecord;

import lombok.Getter;

@Getter
public abstract class AbstractStatistics {

	protected Map<RecordType, BigDecimal> amounts;

	protected Map<RecordType, BigDecimal> sumNormalRecords(Collection<NormalRecord> normalRecords) {
		Map<RecordType, List<NormalRecord>> groupedRecords = normalRecords.stream()
				.collect(Collectors.groupingBy(NormalRecord::getType));

		return Arrays.stream(RecordType.values())
				.filter(recordType -> !RecordHandlingType.TRANSFERING.equals(recordType.getHandlingType()))
				.collect(Collectors.toMap(Function.identity(), recordType -> {
					List<NormalRecord> records = groupedRecords.get(recordType);
					if (CollectionUtils.isEmpty(records)) {
						return new BigDecimal(0);
					} else {
						Optional<BigDecimal> amountOptional = records.stream().map(NormalRecord::getAmount)
								.reduce(BigDecimal::add);

						return amountOptional.orElse(new BigDecimal(0));
					}
				}));
	}
}
