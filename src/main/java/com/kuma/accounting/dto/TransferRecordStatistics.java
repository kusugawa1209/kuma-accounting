package com.kuma.accounting.dto;

import java.math.BigDecimal;

import com.kuma.accounting.message.enums.AccountType;
import com.kuma.accounting.model.BaseAccount;
import com.kuma.accounting.model.TransferRecord;

import lombok.Value;

@Value
public class TransferRecordStatistics {

	private AccountType sourceAccountType;

	private String sourceAccountName;

	private AccountType targetAccountType;

	private String targetAccountName;

	private String description;

	private BigDecimal amount;

	public TransferRecordStatistics(TransferRecord transferRecord) {
		BaseAccount sourceAccount = transferRecord.getSourceAccount();
		this.sourceAccountType = sourceAccount.getType();
		this.sourceAccountName = sourceAccount.getName();

		BaseAccount targetAccount = transferRecord.getTargetAccount();
		this.targetAccountType = targetAccount.getType();
		this.targetAccountName = targetAccount.getName();

		this.description = transferRecord.getDescription();
		this.amount = transferRecord.getAmount();
	}

}
