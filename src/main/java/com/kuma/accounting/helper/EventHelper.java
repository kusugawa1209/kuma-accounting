package com.kuma.accounting.helper;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.FollowEvent;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.profile.UserProfileResponse;

@Component
public class EventHelper {

	@Autowired
	private LineMessagingClient lineMessagingClient;

	public String getUserId(Event event) {
		return event.getSource().getSenderId();
	}

	public String getUsername(Event event) throws InterruptedException, ExecutionException {
		String userId = getUserId(event);

		CompletableFuture<UserProfileResponse> userProfileFuture = lineMessagingClient.getProfile(userId);
		UserProfileResponse userProfileResponse = userProfileFuture.get();

		return userProfileResponse.getDisplayName();
	}

	public String getReplyToken(FollowEvent event) {
		return event.getReplyToken();
	}

	public String getReplyToken(MessageEvent<?> event) {
		return event.getReplyToken();
	}
}
