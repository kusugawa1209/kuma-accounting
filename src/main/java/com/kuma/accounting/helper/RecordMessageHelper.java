package com.kuma.accounting.helper;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.kuma.accounting.message.enums.AccountType;
import com.kuma.accounting.message.enums.RecordMessage;
import com.kuma.accounting.model.Account;
import com.kuma.accounting.model.BaseAccount;
import com.kuma.accounting.model.CreditCard;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.service.AccountService;
import com.kuma.accounting.service.CreditCardService;
import com.kuma.accounting.utils.line.BoxUtils;
import com.kuma.accounting.utils.line.BubbleUtils;
import com.kuma.accounting.utils.line.ButtonUtils;
import com.kuma.accounting.utils.line.FlexMessageUtils;
import com.kuma.accounting.utils.line.TextUtils;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;

@Component
public class RecordMessageHelper {

	@Autowired
	private AccountService accountService;

	@Autowired
	private CreditCardService creditCardService;

	@Autowired
	private MessageHelper messageHelper;

	public void replyChooseTypeMessage(String replyToken) {
		String altText = RecordMessage.CHOOSE_TYPE.getTitle();

		Box body = BoxUtils.newRecordTypeButtonsBody();

		FlexMessage message = FlexMessageUtils.newMessageWithCancelFooter(altText, body);
		messageHelper.reply(replyToken, message);
	}

	public void replyChooseAccountMessage(Member member, String replyToken, RecordMessage nextStep) {
		List<Bubble> bubbles = Lists.newArrayList();

		Box header = BoxUtils.newHeader(nextStep.getTitle());
		Box footer = BoxUtils.newCancelFooter();

		appendAccountBubble(member, bubbles, header, footer);
		appendCreditCardBubble(member, bubbles, header, footer);

		FlexMessage message = FlexMessageUtils.newCarouselMessage(nextStep.getMessage(), bubbles);
		messageHelper.reply(replyToken, message);
	}

	private void appendAccountBubble(Member member, List<Bubble> bubbles, Box header, Box footer) {
		List<Account> accounts = accountService.findByUserId(member.getId());
		if (CollectionUtils.isNotEmpty(accounts)) {
			List<Bubble> accountBubbles = generateButtonsBubbles(header, footer, AccountType.ACCOUNT, accounts);
			bubbles.addAll(accountBubbles);
		}
	}

	private void appendCreditCardBubble(Member member, List<Bubble> bubbles, Box header, Box footer) {
		List<CreditCard> creditCards = creditCardService.findByUserId(member.getId());
		if (CollectionUtils.isNotEmpty(creditCards)) {
			List<Bubble> creditCardBubbles = generateButtonsBubbles(header, footer, AccountType.CREDIT_CARD,
					creditCards);
			bubbles.addAll(creditCardBubbles);
		}
	}

	private List<Bubble> generateButtonsBubbles(Box header, Box footer, AccountType accountType,
			List<? extends BaseAccount> baseAccounts) {

		List<Button> allButtons = baseAccounts.stream().map(account -> ButtonUtils.newButton(account.getName(),
				account.getType().getName() + " " + account.getName())).collect(Collectors.toList());
		List<List<Button>> seperatedButtons = Lists.partition(allButtons, 5);

		return seperatedButtons.stream().map(buttons -> {
			List<FlexComponent> components = Lists.newArrayList();

			Text accountText = TextUtils.newBodyText(accountType.getName());
			components.add(accountText);

			components.addAll(buttons);

			Box body = BoxUtils.newBody(components);

			return BubbleUtils.newBubble(header, body, footer);
		}).collect(Collectors.toList());

	}

}
