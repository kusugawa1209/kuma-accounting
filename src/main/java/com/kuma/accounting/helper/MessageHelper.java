package com.kuma.accounting.helper;

import static com.kuma.accounting.message.enums.Message.ERROR_MESSAGE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

@Component
public class MessageHelper {

	@Autowired
	private LineMessagingClient lineMessagingClient;

	public void replyError(String replyToken) {
		reply(replyToken, ERROR_MESSAGE.getText());
	}

	public void reply(String replyToken, String message) {
		TextMessage textMessage = new TextMessage(message);
		reply(replyToken, textMessage);
	}

	public void reply(String replyToken, Message message) {
		ReplyMessage replyMessage = new ReplyMessage(replyToken, message);
		lineMessagingClient.replyMessage(replyMessage);
	}
}
