package com.kuma.accounting.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import com.kuma.accounting.model.CreditCard;
import com.kuma.accounting.model.Member;

public interface CreditCardService {

	CreditCard createCreditCard(Member member, String name, BigDecimal quota, Integer dueDate, Integer billingDate,
			BigDecimal initialAmount);

	Optional<CreditCard> findByUserIdAndCreditCardName(String userId, String creditCardName);

	List<CreditCard> findByUserId(String userId);

	CreditCard getByUserIdAndCreditCardId(String userId, long creditCardId);

	void removeCreditCard(Member member, CreditCard creditCard);

	CreditCard saveCreditCard(CreditCard creditCard);

}
