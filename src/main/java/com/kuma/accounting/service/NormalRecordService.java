package com.kuma.accounting.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.kuma.accounting.message.enums.RecordType;
import com.kuma.accounting.model.BaseAccount;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.model.NormalRecord;

public interface NormalRecordService {

	NormalRecord createRecord(Member member, BaseAccount account, RecordType recordType, Date date, String description,
			BigDecimal amount);

	List<NormalRecord> findRecordsByMemberAndYearAndMonth(Member member, Date date);
	
	List<NormalRecord> findRecordsByMemberAndDate(Member member, Date date);

}
