package com.kuma.accounting.service;

import java.util.Optional;

import com.kuma.accounting.model.Member;

public interface MemberService {

	Member createMember(String id, String username);

	Optional<Member> findMember(String id);

	Member saveMember(Member member);

	void deleteMember(String id);

	void resetProcess(Member member);

}
