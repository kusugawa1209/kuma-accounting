package com.kuma.accounting.service;

import java.util.Date;

import com.kuma.accounting.dto.DayStatistics;
import com.kuma.accounting.dto.MonthStatistics;
import com.kuma.accounting.model.Member;

public interface StatisticsService {

	MonthStatistics getMonthStatistics(Member member, Date date);

	DayStatistics getDayStatistics(Member member, Date date);

}
