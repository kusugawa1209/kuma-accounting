package com.kuma.accounting.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.kuma.accounting.model.BaseAccount;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.model.TransferRecord;

public interface TransferRecordService {

	TransferRecord createRecord(Member member, BaseAccount sourceAccount, BaseAccount targetAccount, Date date,
			String description, BigDecimal amount);
	
	List<TransferRecord> findRecordsByMemberAndDate(Member member, Date date);
}
