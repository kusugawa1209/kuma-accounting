package com.kuma.accounting.service.impl;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kuma.accounting.model.Account;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.repository.AccountRepository;
import com.kuma.accounting.service.AccountService;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountRepository repository;

	@Override
	public Account createAccount(Member member, String name, BigDecimal initialAmount) {
		Account account = new Account(name, true, member, initialAmount);

		return saveAccount(account);
	}

	@Override
	public Optional<Account> findByUserIdAndAccountName(String userId, String accountName) {
		return repository.findByMemberIdAndName(userId, accountName);
	}

	@Override
	public List<Account> findByUserId(String userId) {
		List<Account> accounts = repository.findByMemberId(userId);
		Collections.sort(accounts);

		return accounts;
	}

	@Override
	public void removeAccount(Member member, Account account) {
		if (member.equals(account.getMember())) {
			repository.delete(account);
		}
	}

	@Override
	public Account getByUserIdAndAccountId(String userId, long accountId) {
		return repository.getByMemberIdAndId(userId, accountId);
	}

	@Override
	public Account saveAccount(Account account) {
		Long currentCount = account.getCount();
		Long newCount = Objects.isNull(currentCount) ? 1 : currentCount + 1L;
		account.setCount(newCount);

		return repository.save(account);
	}

}
