package com.kuma.accounting.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kuma.accounting.model.Member;
import com.kuma.accounting.repository.MemberRepository;
import com.kuma.accounting.service.MemberService;

@Service
@Transactional
public class MemberServiceImpl implements MemberService {

	@Autowired
	private MemberRepository repository;

	@Override
	public Member createMember(String id, String username) {
		Optional<Member> memberOptional = findMember(id);
		if (memberOptional.isPresent()) {
			Member member = memberOptional.get();
			member.setIsActive(true);

			return saveMember(member);
		} else {
			Member member = new Member(id, username, true);

			return saveMember(member);
		}
	}

	@Override
	public Optional<Member> findMember(String id) {
		return repository.findById(id);
	}

	@Override
	public Member saveMember(Member member) {
		return repository.save(member);
	}

	@Override
	public void deleteMember(String id) {
		Optional<Member> memberOptional = findMember(id);
		memberOptional.ifPresent(member -> {
			member.setIsActive(false);
			saveMember(member);
		});

	}

	@Override
	public void resetProcess(Member member) {
		member.setProcess(null);
		member.setParameters(null);

		saveMember(member);
	}
}
