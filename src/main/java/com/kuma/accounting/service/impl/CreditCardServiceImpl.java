package com.kuma.accounting.service.impl;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kuma.accounting.model.CreditCard;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.repository.CreditCardRepository;
import com.kuma.accounting.service.CreditCardService;

@Service
@Transactional
public class CreditCardServiceImpl implements CreditCardService {

	@Autowired
	private CreditCardRepository repository;

	@Override
	public CreditCard createCreditCard(Member member, String name, BigDecimal quota, Integer dueDate,
			Integer billingDate, BigDecimal initialAmount) {
		CreditCard creditCard = new CreditCard(name, true, quota, dueDate, billingDate, initialAmount, member);

		return saveCreditCard(creditCard);
	}

	@Override
	public Optional<CreditCard> findByUserIdAndCreditCardName(String userId, String creditCardName) {
		return repository.findByMemberIdAndName(userId, creditCardName);
	}

	@Override
	public List<CreditCard> findByUserId(String userId) {
		List<CreditCard> creditCards = repository.findByMemberId(userId);
		Collections.sort(creditCards);

		return creditCards;
	}

	@Override
	public void removeCreditCard(Member member, CreditCard creditCard) {
		if (member.equals(creditCard.getMember())) {
			repository.delete(creditCard);
		}
	}

	@Override
	public CreditCard getByUserIdAndCreditCardId(String userId, long creditCardId) {
		return repository.getByMemberIdAndId(userId, creditCardId);
	}

	@Override
	public CreditCard saveCreditCard(CreditCard creditCard) {
		Long currentCount = creditCard.getCount();
		Long newCount = Objects.isNull(currentCount) ? 1 : currentCount + 1L;
		creditCard.setCount(newCount);

		return repository.save(creditCard);
	}

}
