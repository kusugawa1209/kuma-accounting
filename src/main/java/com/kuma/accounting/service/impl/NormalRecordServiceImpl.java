package com.kuma.accounting.service.impl;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kuma.accounting.message.enums.AccountType;
import com.kuma.accounting.message.enums.RecordHandlingType;
import com.kuma.accounting.message.enums.RecordType;
import com.kuma.accounting.model.Account;
import com.kuma.accounting.model.BaseAccount;
import com.kuma.accounting.model.CreditCard;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.model.NormalRecord;
import com.kuma.accounting.repository.NormalRecordRepository;
import com.kuma.accounting.service.AccountService;
import com.kuma.accounting.service.CreditCardService;
import com.kuma.accounting.service.NormalRecordService;

@Service
@Transactional
public class NormalRecordServiceImpl implements NormalRecordService {

	@Autowired
	private NormalRecordRepository repository;

	@Autowired
	private AccountService accountService;

	@Autowired
	private CreditCardService creditCardService;

	@Override
	public NormalRecord createRecord(Member member, BaseAccount baseAccount, RecordType recordType, Date date,
			String description, BigDecimal amount) {
		AccountType accountType = baseAccount.getType();
		String userId = member.getId();
		Long baseAccountId = baseAccount.getId();
		BigDecimal currentAmount = baseAccount.getCurrentAmount();

		BigDecimal newAmount = handleAmount(recordType, accountType, currentAmount, amount);

		if (AccountType.ACCOUNT.equals(accountType)) {
			Account account = accountService.getByUserIdAndAccountId(userId, baseAccountId);
			account.setCurrentAmount(newAmount);

			baseAccount = accountService.saveAccount(account);
		} else if (AccountType.CREDIT_CARD.equals(accountType)) {
			CreditCard creditCard = creditCardService.getByUserIdAndCreditCardId(userId, baseAccountId);
			creditCard.setCurrentAmount(newAmount);

			baseAccount = creditCardService.saveCreditCard(creditCard);
		}

		NormalRecord record = new NormalRecord(member, baseAccount, recordType, date, description, amount);
		return repository.save(record);
	}

	@Override
	public List<NormalRecord> findRecordsByMemberAndYearAndMonth(Member member, Date date) {
		List<NormalRecord> normalRecords = repository.findRecordsByMemberAndYearAndMonth(member.getId(), date);
		Collections.sort(normalRecords);

		return normalRecords;
	}

	@Override
	public List<NormalRecord> findRecordsByMemberAndDate(Member member, Date date) {
		List<NormalRecord> normalRecords = repository.findRecordsByMemberAndYearAndMonthAndDay(member.getId(), date);
		Collections.sort(normalRecords);

		return normalRecords;
	}

	private BigDecimal handleAmount(RecordType recordType, AccountType accountType, BigDecimal currentAmount,
			BigDecimal appendAmount) {
		RecordHandlingType handlingType = recordType.getHandlingType();
		if (AccountType.ACCOUNT.equals(accountType)) {
			if (RecordHandlingType.ADDING.equals(handlingType)) {
				return currentAmount.add(appendAmount);
			} else if (RecordHandlingType.SUBSTRACTING.equals(handlingType)) {
				return currentAmount.subtract(appendAmount);
			} else {
				return currentAmount;
			}
		} else if (AccountType.CREDIT_CARD.equals(accountType)) {
			if (RecordHandlingType.ADDING.equals(handlingType)) {
				return currentAmount.subtract(appendAmount);
			} else if (RecordHandlingType.SUBSTRACTING.equals(handlingType)) {
				return currentAmount.add(appendAmount);
			} else {
				return currentAmount;
			}
		} else {
			throw new UnsupportedOperationException(accountType.getName() + " 尚未實作");
		}
	}
}
