package com.kuma.accounting.service.impl;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuma.accounting.message.enums.AccountType;
import com.kuma.accounting.model.Account;
import com.kuma.accounting.model.BaseAccount;
import com.kuma.accounting.model.CreditCard;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.model.TransferRecord;
import com.kuma.accounting.repository.TransferRecordRepository;
import com.kuma.accounting.service.AccountService;
import com.kuma.accounting.service.CreditCardService;
import com.kuma.accounting.service.TransferRecordService;

@Service
public class TransferRecordServiceImpl implements TransferRecordService {

	@Autowired
	private TransferRecordRepository repository;

	@Autowired
	private AccountService accountService;

	@Autowired
	private CreditCardService creditCardService;

	@Override
	public TransferRecord createRecord(Member member, BaseAccount sourceBaseAccount, BaseAccount targetBaseAccount,
			Date date, String description, BigDecimal amount) {
		String userId = member.getId();
		AccountType sourceAccountType = sourceBaseAccount.getType();
		Long sourceBaseAccountId = sourceBaseAccount.getId();
		BigDecimal sourceCurrentAmount = sourceBaseAccount.getCurrentAmount();

		AccountType targetAccountType = targetBaseAccount.getType();
		Long targetBaseAccountId = targetBaseAccount.getId();
		BigDecimal targetCurrentAmount = targetBaseAccount.getCurrentAmount();

		if (AccountType.ACCOUNT.equals(sourceAccountType)) {
			sourceCurrentAmount = sourceCurrentAmount.subtract(amount);
			Account sourceAccount = accountService.getByUserIdAndAccountId(userId, sourceBaseAccountId);
			sourceAccount.setCurrentAmount(sourceCurrentAmount);

			sourceBaseAccount = accountService.saveAccount(sourceAccount);
		} else if (AccountType.CREDIT_CARD.equals(sourceAccountType)) {
			sourceCurrentAmount = sourceCurrentAmount.add(amount);
			CreditCard sourceCreditCard = creditCardService.getByUserIdAndCreditCardId(userId, sourceBaseAccountId);
			sourceCreditCard.setCurrentAmount(sourceCurrentAmount);

			sourceBaseAccount = creditCardService.saveCreditCard(sourceCreditCard);
		}

		if (AccountType.ACCOUNT.equals(targetAccountType)) {
			targetCurrentAmount = targetCurrentAmount.add(amount);
			Account targetAccount = accountService.getByUserIdAndAccountId(userId, targetBaseAccountId);
			targetAccount.setCurrentAmount(targetCurrentAmount);

			targetBaseAccount = accountService.saveAccount(targetAccount);
		} else if (AccountType.CREDIT_CARD.equals(targetAccountType)) {
			targetCurrentAmount = targetCurrentAmount.subtract(amount);
			CreditCard targetCreditCard = creditCardService.getByUserIdAndCreditCardId(userId, targetBaseAccountId);
			targetCreditCard.setCurrentAmount(targetCurrentAmount);

			targetBaseAccount = creditCardService.saveCreditCard(targetCreditCard);
		}

		TransferRecord record = new TransferRecord(member, sourceBaseAccount, targetBaseAccount, date, description,
				amount);
		return repository.save(record);
	}

	@Override
	public List<TransferRecord> findRecordsByMemberAndDate(Member member, Date date) {
		List<TransferRecord> transferRecords = repository.findRecordsByMemberAndYearAndMonthAndDay(member.getId(),
				date);
		Collections.sort(transferRecords);

		return transferRecords;
	}

}
