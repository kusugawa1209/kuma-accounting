package com.kuma.accounting.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kuma.accounting.dto.DayStatistics;
import com.kuma.accounting.dto.MonthStatistics;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.model.NormalRecord;
import com.kuma.accounting.model.TransferRecord;
import com.kuma.accounting.service.NormalRecordService;
import com.kuma.accounting.service.StatisticsService;
import com.kuma.accounting.service.TransferRecordService;

@Service
@Transactional
public class StatisticsServiceImpl implements StatisticsService {

	@Autowired
	private NormalRecordService normalRecordService;

	@Autowired
	private TransferRecordService transferRecordService;

	@Override
	public MonthStatistics getMonthStatistics(Member member, Date date) {
		List<NormalRecord> normalRecords = normalRecordService.findRecordsByMemberAndYearAndMonth(member, date);

		return new MonthStatistics(date, normalRecords);
	}

	@Override
	public DayStatistics getDayStatistics(Member member, Date date) {
		List<NormalRecord> normalRecords = normalRecordService.findRecordsByMemberAndDate(member, date);
		List<TransferRecord> transferRecords = transferRecordService.findRecordsByMemberAndDate(member, date);

		return new DayStatistics(date, normalRecords, transferRecords);
	}
}
