package com.kuma.accounting.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import com.kuma.accounting.model.Account;
import com.kuma.accounting.model.Member;

public interface AccountService {

	Account createAccount(Member member, String name, BigDecimal initialAmount);

	Optional<Account> findByUserIdAndAccountName(String userId, String accountName);

	List<Account> findByUserId(String userId);

	Account getByUserIdAndAccountId(String userId, long accountId);

	void removeAccount(Member member, Account account);

	Account saveAccount(Account account);

}
