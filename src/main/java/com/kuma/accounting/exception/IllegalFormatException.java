package com.kuma.accounting.exception;

public class IllegalFormatException extends Exception {

	private static final long serialVersionUID = 1L;

	public IllegalFormatException(String message) {
		super(message);
	}

	public IllegalFormatException(Throwable e) {
		super(e);
	}

}
