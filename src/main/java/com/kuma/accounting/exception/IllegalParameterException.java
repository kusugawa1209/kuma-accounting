package com.kuma.accounting.exception;

public class IllegalParameterException extends Exception {

	private static final long serialVersionUID = 1L;

	public IllegalParameterException(String message) {
		super(message);
	}

	public IllegalParameterException(Throwable e) {
		super(e);
	}

}
