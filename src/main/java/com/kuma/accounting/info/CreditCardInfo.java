package com.kuma.accounting.info;

import java.math.BigDecimal;

public interface CreditCardInfo extends AccountInfo {

	public BigDecimal getCurrentAmount();

	public BigDecimal getQuota();

	public Integer getDueDate();

	public Integer getBillingDate();

}
