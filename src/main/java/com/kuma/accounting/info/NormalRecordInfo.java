package com.kuma.accounting.info;

import java.math.BigDecimal;
import java.util.Date;

import com.kuma.accounting.message.enums.RecordType;

public interface NormalRecordInfo {
	public BaseAccountInfo getAccount();

	public RecordType getType();

	public Date getDate();

	public String getDescription();

	public BigDecimal getAmount();
}
