package com.kuma.accounting.info;

import java.math.BigDecimal;

public interface AccountInfo extends BaseAccountInfo {

	public BigDecimal getCurrentAmount();
	
}
