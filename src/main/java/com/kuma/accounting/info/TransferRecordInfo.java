package com.kuma.accounting.info;

import java.math.BigDecimal;
import java.util.Date;

public interface TransferRecordInfo {
	public BaseAccountInfo getSourceAccount();

	public BaseAccountInfo getTargetAccount();

	public Date getDate();

	public String getDescription();

	public BigDecimal getAmount();
}
