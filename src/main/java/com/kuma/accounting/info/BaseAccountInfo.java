package com.kuma.accounting.info;

import com.kuma.accounting.message.enums.AccountType;

public interface BaseAccountInfo {

	public AccountType getType();

	public String getName();

}
