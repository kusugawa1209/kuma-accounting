package com.kuma.accounting.handler.creditcard;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.accounting.dto.CreditCardParameter;
import com.kuma.accounting.handler.process.AbstractHandler;
import com.kuma.accounting.handler.process.ProcessHandler;
import com.kuma.accounting.message.enums.CreditCardMessage;
import com.kuma.accounting.message.enums.Message;
import com.kuma.accounting.model.CreditCard;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.utils.line.FlexMessageUtils;
import com.linecorp.bot.model.message.FlexMessage;

@Component
public class CreditCardNameHandler extends AbstractHandler {

	@Autowired
	private CreditCardQuotaHandler handler;

	@Override
	protected boolean shouldHandle(Member member, String parameter) {
		CreditCardParameter parameters = member.getParameter(CreditCardParameter.class);
		return StringUtils.isBlank(parameters.getCreditCardName());
	}

	@Override
	protected void tryHandle(String replyToken, Member member, String parameter) {
		Optional<CreditCard> existedCreditCardOptional = creditCardService.findByUserIdAndCreditCardName(member.getId(),
				parameter);
		if (existedCreditCardOptional.isPresent()) {
			FlexMessage message = FlexMessageUtils.newMessageWithCancelFooter(Message.CREDIT_CARD_NAME_EXISTS.getText(),
					CreditCardMessage.CREDIT_CARD_NAME.getMessage());

			messageHelper.reply(replyToken, message);
		} else {
			CreditCardParameter parameters = member.getParameter(CreditCardParameter.class);
			parameters.setCreditCardName(parameter);

			member.setParameter(parameters);
			memberService.saveMember(member);

			CreditCardMessage nextStep = CreditCardMessage.QUOTA;

			FlexMessage message = FlexMessageUtils.newMessageWithCancelFooter(nextStep.getTitle(),
					nextStep.getMessage());
			messageHelper.reply(replyToken, message);
		}
	}

	@Override
	protected Optional<ProcessHandler> getNextHandler(Member member) {
		return Optional.of(handler);
	}

}
