package com.kuma.accounting.handler.creditcard;

import static com.kuma.accounting.message.enums.Message.NO_CREDIT_CARD_TEXT;
import static com.kuma.accounting.message.enums.Message.NO_CREDIT_CARD_TITLE;
import static com.kuma.accounting.message.enums.Process.ADD_CREDIT_CARD;
import static com.kuma.accounting.message.enums.Process.CHECK_CREDIT_CARD;
import static com.kuma.accounting.message.enums.Process.REMOVE_CREDIT_CARD;
import static com.kuma.accounting.message.enums.Process.SHOW_FUNCTION_MENU;
import static com.kuma.accounting.utils.line.BoxUtils.newBody;
import static com.kuma.accounting.utils.line.BoxUtils.newCreditCardInfoBody;
import static com.kuma.accounting.utils.line.BoxUtils.newFooter;
import static com.kuma.accounting.utils.line.BoxUtils.newHeader;
import static com.kuma.accounting.utils.line.BubbleUtils.newBubble;
import static com.kuma.accounting.utils.line.ButtonUtils.newButton;
import static com.kuma.accounting.utils.line.FlexMessageUtils.newCarouselMessage;
import static com.kuma.accounting.utils.line.FlexMessageUtils.newMessage;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.accounting.handler.line.AbstractMessageEventHandler;
import com.kuma.accounting.message.enums.Process;
import com.kuma.accounting.model.CreditCard;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.service.CreditCardService;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.container.Bubble;

@Component
public class CreditCardCheckingHandler extends AbstractMessageEventHandler {

	@Autowired
	private CreditCardService creditCardService;

	@Override
	protected Process getSupportedProcess() {
		return CHECK_CREDIT_CARD;
	}

	@Override
	protected void handleTextEvent(String replyToken, Member member, String parameter) {
		memberService.resetProcess(member);

		FlexMessage message;
		List<CreditCard> creditCards = creditCardService.findByUserId(member.getId());
		if (isNotEmpty(creditCards)) {
			message = getCreditCardsMessage(creditCards);
		} else {
			message = getNoCreditCardMessage();
		}

		messageHelper.reply(replyToken, message);
	}

	private FlexMessage getNoCreditCardMessage() {
		Box header = newHeader(NO_CREDIT_CARD_TITLE.getText());
		Box body = newBody(NO_CREDIT_CARD_TEXT.getText());

		Button checkCreditCardButton = newButton(ADD_CREDIT_CARD.getText());
		Button functionMenuButton = newButton(SHOW_FUNCTION_MENU.getText());
		Box footer = newFooter(checkCreditCardButton, functionMenuButton);

		return newMessage(NO_CREDIT_CARD_TITLE.getText(), header, body, footer);
	}

	private FlexMessage getCreditCardsMessage(List<CreditCard> creditCards) {
		List<Bubble> bubbles = creditCards.stream().map(creditCard -> {
			String creditCardName = creditCard.getName();
			Box header = newHeader(creditCardName);
			Box body = newCreditCardInfoBody(creditCard);

			Button removeCreditCardButton = newButton(REMOVE_CREDIT_CARD.getText(),
					REMOVE_CREDIT_CARD.getText() + " " + creditCardName);
			Button functionMenuButton = newButton(Process.SHOW_FUNCTION_MENU.getText());
			Box footer = newFooter(removeCreditCardButton, functionMenuButton);

			return newBubble(header, body, footer);
		}).collect(toList());

		return newCarouselMessage(CHECK_CREDIT_CARD.getText(), bubbles);
	}
}
