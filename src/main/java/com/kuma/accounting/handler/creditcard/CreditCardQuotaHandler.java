package com.kuma.accounting.handler.creditcard;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.accounting.dto.CreditCardParameter;
import com.kuma.accounting.handler.process.AbstractHandler;
import com.kuma.accounting.handler.process.ProcessHandler;
import com.kuma.accounting.message.enums.CreditCardMessage;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.utils.line.FlexMessageUtils;
import com.linecorp.bot.model.message.FlexMessage;

@Component
public class CreditCardQuotaHandler extends AbstractHandler {

	@Autowired
	private CreditCardDueDateHandler handler;

	@Override
	protected boolean shouldHandle(Member member, String parameter) {
		CreditCardParameter parameters = member.getParameter(CreditCardParameter.class);

		return Objects.isNull(parameters.getQuota());
	}

	@Override
	protected void tryHandle(String replyToken, Member member, String parameter) {
		CreditCardParameter parameters = member.getParameter(CreditCardParameter.class);

		try {
			BigDecimal quota = new BigDecimal(parameter);
			parameters.setQuota(quota);

			member.setParameter(parameters);
			memberService.saveMember(member);

			CreditCardMessage nextStep = CreditCardMessage.DUE_DATE;

			FlexMessage message = FlexMessageUtils.newMessageWithCancelFooter(nextStep.getTitle(),
					nextStep.getMessage());
			messageHelper.reply(replyToken, message);
		} catch (NumberFormatException e) {
			CreditCardMessage currentStep = CreditCardMessage.QUOTA;

			FlexMessage message = FlexMessageUtils.newMessageWithCancelFooter(currentStep.getTitle(),
					currentStep.getMessage());
			messageHelper.reply(replyToken, message);
		}
	}

	@Override
	protected Optional<ProcessHandler> getNextHandler(Member member) {
		return Optional.of(handler);
	}

}
