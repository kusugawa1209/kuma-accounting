package com.kuma.accounting.handler.creditcard;

import java.math.BigDecimal;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.accounting.dto.CreditCardParameter;
import com.kuma.accounting.handler.process.AbstractHandler;
import com.kuma.accounting.handler.process.ProcessHandler;
import com.kuma.accounting.message.enums.Message;
import com.kuma.accounting.message.enums.Process;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.utils.line.BoxUtils;
import com.kuma.accounting.utils.line.ButtonUtils;
import com.kuma.accounting.utils.line.FlexMessageUtils;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;

@Component
public class CreditCardConfirmHandler extends AbstractHandler {

	@Autowired
	private CreditCardReConfirmHandler handler;

	@Override
	protected boolean shouldHandle(Member member, String parameter) {
		return parameter.equals(Message.CONFIRM.getText());
	}

	@Override
	protected void tryHandle(String replyToken, Member member, String parameter) {
		CreditCardParameter parameters = member.getParameter(CreditCardParameter.class);

		String creditCardName = parameters.getCreditCardName();
		BigDecimal quota = parameters.getQuota();
		Integer dueDate = parameters.getDueDate();
		Integer billingDate = parameters.getBillingDate();
		BigDecimal initialAmount = parameters.getInitialAmount();

		creditCardService.createCreditCard(member, creditCardName, quota, dueDate, billingDate, initialAmount);
		memberService.resetProcess(member);

		Box header = BoxUtils.newHeader(Message.CREDIT_CARD_SAVED.getText());
		Box body = BoxUtils.newCreditCardInfoBody(parameters);

		Button addCreditCardButton = ButtonUtils.newButton(Process.ADD_CREDIT_CARD.getText());
		Button functionMenuButton = ButtonUtils.newButton(Process.SHOW_FUNCTION_MENU.getText());
		Box footer = BoxUtils.newFooter(addCreditCardButton, functionMenuButton);

		FlexMessage message = FlexMessageUtils.newMessage(Message.CREDIT_CARD_SAVED.getText(), header, body, footer);
		messageHelper.reply(replyToken, message);
	}

	@Override
	protected Optional<ProcessHandler> getNextHandler(Member member) {
		return Optional.of(handler);
	}
}
