package com.kuma.accounting.handler.creditcard;

import static com.kuma.accounting.message.enums.Message.CONFIRM;
import static com.kuma.accounting.message.enums.Message.CONFIRM_REMOVE_CREDIT_CARD;
import static com.kuma.accounting.message.enums.Message.CREDIT_CARD_NAME_NOT_FOUND;
import static com.kuma.accounting.message.enums.Message.CREDIT_CARD_REMOVED;
import static com.kuma.accounting.message.enums.Message.NO_CREDIT_CARD_FOUND;
import static com.kuma.accounting.message.enums.Message.REMOVED;
import static com.kuma.accounting.message.enums.Process.ADD_CREDIT_CARD;
import static com.kuma.accounting.message.enums.Process.REMOVE_CREDIT_CARD;
import static com.kuma.accounting.message.enums.Process.SHOW_FUNCTION_MENU;
import static com.kuma.accounting.utils.line.BoxUtils.newBody;
import static com.kuma.accounting.utils.line.BoxUtils.newCancelFooter;
import static com.kuma.accounting.utils.line.BoxUtils.newCreditCardInfoBody;
import static com.kuma.accounting.utils.line.BoxUtils.newFooter;
import static com.kuma.accounting.utils.line.BoxUtils.newHeader;
import static com.kuma.accounting.utils.line.ButtonUtils.newButton;
import static com.kuma.accounting.utils.line.FlexMessageUtils.newConfirmAndCancelMessage;
import static com.kuma.accounting.utils.line.FlexMessageUtils.newMessage;
import static com.kuma.accounting.utils.line.TextUtils.newBodyText;
import static java.util.Optional.empty;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.kuma.accounting.handler.line.AbstractMessageEventHandler;
import com.kuma.accounting.message.enums.Process;
import com.kuma.accounting.model.CreditCard;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.service.CreditCardService;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Text;

@Component
public class CreditCardRemovingHandler extends AbstractMessageEventHandler {

	@Autowired
	private CreditCardService creditCardService;

	@Autowired
	private CreditCardCheckingHandler creditCardCheckingHandler;

	@Override
	protected Process getSupportedProcess() {
		return REMOVE_CREDIT_CARD;
	}

	@Override
	protected void handleTextEvent(String replyToken, Member member, String parameter) {
		Optional<Process> processOptional = member.getProcess();
		if (!processOptional.isPresent()) {
			startRemovingCreditCardProcess(replyToken, member, parameter);
		} else {
			runRemovingCreditCardParametersProcess(replyToken, member, parameter);
		}
	}

	private void runRemovingCreditCardParametersProcess(String replyToken, Member member, String parameter) {
		Optional<CreditCard> creditCardOptional = getCreditCard(member);
		if (!creditCardOptional.isPresent()) {
			creditCardOptional = creditCardService.findByUserIdAndCreditCardName(member.getId(), parameter);
			if (creditCardOptional.isPresent()) {
				CreditCard creditCard = creditCardOptional.get();
				member.setParameters(parameter);
				memberService.saveMember(member);

				replyConfirmRemovingCreditCardMessage(replyToken, creditCard);
			} else {
				Box header = newHeader(NO_CREDIT_CARD_FOUND.getText());

				List<FlexComponent> bodyComponents = Lists.newArrayList();
				Text notFoundText = newBodyText(CREDIT_CARD_NAME_NOT_FOUND.getText());
				bodyComponents.add(notFoundText);

				List<CreditCard> creditCards = creditCardService.findByUserId(member.getId());
				List<Button> creditCardButtons = creditCards.stream().map(creditCard -> {
					String creditCardName = creditCard.getName();
					return newButton(creditCardName);
				}).collect(Collectors.toList());
				bodyComponents.addAll(creditCardButtons);

				Box body = newBody(bodyComponents);

				Box footer = newCancelFooter();

				FlexMessage message = newMessage(NO_CREDIT_CARD_FOUND.getText(), header, body, footer);
				messageHelper.reply(replyToken, message);
			}
		} else {
			CreditCard creditCard = creditCardOptional.get();
			if (parameter.equals(CONFIRM.getText())) {
				removeCreditCard(replyToken, member, creditCard);
			} else {
				replyConfirmRemovingCreditCardMessage(replyToken, creditCard);
			}
		}
	}

	private void replyConfirmRemovingCreditCardMessage(String replyToken, CreditCard creditCard) {
		Box creditCardInfoBody = newCreditCardInfoBody(creditCard);

		FlexMessage message = newConfirmAndCancelMessage(CONFIRM_REMOVE_CREDIT_CARD.getText(), creditCardInfoBody);
		messageHelper.reply(replyToken, message);
	}

	private void removeCreditCard(String replyToken, Member member, CreditCard creditCard) {
		creditCardService.removeCreditCard(member, creditCard);
		memberService.resetProcess(member);

		Box header = newHeader(CREDIT_CARD_REMOVED.getText());
		Box body = newBody(REMOVED.getText() + creditCard.getName());

		Button addCreditCardButton = newButton(ADD_CREDIT_CARD.getText());
		Button functionMenuButton = newButton(SHOW_FUNCTION_MENU.getText());
		Box footer = newFooter(addCreditCardButton, functionMenuButton);

		FlexMessage message = newMessage(CREDIT_CARD_REMOVED.getText(), header, body, footer);
		messageHelper.reply(replyToken, message);
	}

	private void startRemovingCreditCardProcess(String replyToken, Member member, String parameter) {
		if (StringUtils.equalsIgnoreCase(parameter, REMOVE_CREDIT_CARD.getText())) {
			creditCardCheckingHandler.handleTextEvent(replyToken, member, parameter);
		} else if (StringUtils.startsWithIgnoreCase(parameter, REMOVE_CREDIT_CARD.getText())) {
			member.setProcess(REMOVE_CREDIT_CARD);
			memberService.saveMember(member);

			String creditCardName = StringUtils.substringAfter(parameter, " ");

			runRemovingCreditCardParametersProcess(replyToken, member, creditCardName);
		}
	}

	private Optional<CreditCard> getCreditCard(Member member) {
		Optional<String> parametersOptional = member.getParameters();
		if (parametersOptional.isPresent()) {
			String parameters = parametersOptional.get();
			return creditCardService.findByUserIdAndCreditCardName(member.getId(), parameters);
		} else {
			return empty();
		}
	}
}
