package com.kuma.accounting.handler.creditcard;

import java.util.Optional;

import org.springframework.stereotype.Component;

import com.kuma.accounting.dto.CreditCardParameter;
import com.kuma.accounting.handler.process.AbstractHandler;
import com.kuma.accounting.handler.process.ProcessHandler;
import com.kuma.accounting.message.enums.Message;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.utils.line.BoxUtils;
import com.kuma.accounting.utils.line.FlexMessageUtils;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;

@Component
public class CreditCardReConfirmHandler extends AbstractHandler {

	@Override
	protected boolean shouldHandle(Member member, String parameter) {
		return true;
	}

	@Override
	protected void tryHandle(String replyToken, Member member, String parameter) {
		CreditCardParameter parameters = member.getParameter(CreditCardParameter.class);
		Box creditCardInfoBody = BoxUtils.newCreditCardInfoBody(parameters);
		FlexMessage message = FlexMessageUtils.newConfirmAndCancelMessage(Message.CONFIRM_ADD_CREDIT_CARD.getText(),
				creditCardInfoBody);
		messageHelper.reply(replyToken, message);
	}

	@Override
	protected Optional<ProcessHandler> getNextHandler(Member member) {
		return Optional.empty();
	}

}
