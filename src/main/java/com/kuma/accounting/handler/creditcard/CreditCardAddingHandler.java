package com.kuma.accounting.handler.creditcard;

import static com.kuma.accounting.message.enums.Process.ADD_CREDIT_CARD;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.accounting.handler.line.AbstractMessageEventHandler;
import com.kuma.accounting.message.enums.Process;
import com.kuma.accounting.model.Member;

@Component
public class CreditCardAddingHandler extends AbstractMessageEventHandler {

	@Autowired
	private CreditCardAddingProcessHandler handler;

	@Override
	protected Process getSupportedProcess() {
		return ADD_CREDIT_CARD;
	}

	@Override
	protected void handleTextEvent(String replyToken, Member member, String parameter) {
		handler.handle(replyToken, member, parameter);
	}
}
