package com.kuma.accounting.handler.creditcard;

import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.accounting.dto.CreditCardParameter;
import com.kuma.accounting.handler.process.AbstractHandler;
import com.kuma.accounting.handler.process.ProcessHandler;
import com.kuma.accounting.message.enums.CreditCardMessage;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.utils.line.FlexMessageUtils;
import com.linecorp.bot.model.message.FlexMessage;

@Component
public class CreditCardBillingDateHandler extends AbstractHandler {

	@Autowired
	private CreditCardInitialAmountHandler handler;

	@Override
	protected boolean shouldHandle(Member member, String parameter) {
		CreditCardParameter parameters = member.getParameter(CreditCardParameter.class);

		return Objects.isNull(parameters.getBillingDate());
	}

	@Override
	protected void tryHandle(String replyToken, Member member, String parameter) {
		CreditCardParameter parameters = member.getParameter(CreditCardParameter.class);

		try {
			int billingDate = Integer.parseInt(parameter);
			parameters.setBillingDate(billingDate);

			member.setParameter(parameters);
			memberService.saveMember(member);

			CreditCardMessage nextStep = CreditCardMessage.INITIAL_AMOUNT;

			FlexMessage message = FlexMessageUtils.newMessageWithCancelFooter(nextStep.getTitle(),
					nextStep.getMessage());
			messageHelper.reply(replyToken, message);
		} catch (NumberFormatException e) {
			CreditCardMessage currentStep = CreditCardMessage.BILLING_DATE;

			FlexMessage message = FlexMessageUtils.newMessageWithCancelFooter(currentStep.getTitle(),
					currentStep.getMessage());
			messageHelper.reply(replyToken, message);
		}
	}

	@Override
	protected Optional<ProcessHandler> getNextHandler(Member member) {
		return Optional.of(handler);
	}

}
