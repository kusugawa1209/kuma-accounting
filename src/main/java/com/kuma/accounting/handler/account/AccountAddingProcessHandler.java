package com.kuma.accounting.handler.account;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.accounting.handler.process.AbstractHandler;
import com.kuma.accounting.handler.process.ProcessHandler;
import com.kuma.accounting.message.enums.AccountMessage;
import com.kuma.accounting.message.enums.Process;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.utils.line.FlexMessageUtils;
import com.linecorp.bot.model.message.FlexMessage;

@Component
public class AccountAddingProcessHandler extends AbstractHandler {

	@Autowired
	private AccountNameHandler handler;

	@Override
	protected boolean shouldHandle(Member member, String parameter) {
		Optional<Process> processOptional = member.getProcess();

		return !processOptional.isPresent();
	}

	@Override
	protected void tryHandle(String replyToken, Member member, String parameter) {
		member.setProcess(Process.ADD_ACCOUNT);
		memberService.saveMember(member);

		AccountMessage nextStep = AccountMessage.ACCOUNT_NAME;

		FlexMessage message = FlexMessageUtils.newMessageWithCancelFooter(nextStep.getTitle(), nextStep.getMessage());
		messageHelper.reply(replyToken, message);
	}

	@Override
	protected Optional<ProcessHandler> getNextHandler(Member member) {
		return Optional.of(handler);
	}

}
