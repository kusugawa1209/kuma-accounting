package com.kuma.accounting.handler.account;

import static com.kuma.accounting.message.enums.Message.ACCOUNT_NAME_NOT_FOUND;
import static com.kuma.accounting.message.enums.Message.ACCOUNT_REMOVED;
import static com.kuma.accounting.message.enums.Message.CONFIRM;
import static com.kuma.accounting.message.enums.Message.CONFIRM_REMOVE_ACCOUNT;
import static com.kuma.accounting.message.enums.Message.NO_ACCOUNT_FOUND;
import static com.kuma.accounting.message.enums.Message.REMOVED;
import static com.kuma.accounting.message.enums.Process.ADD_ACCOUNT;
import static com.kuma.accounting.message.enums.Process.REMOVE_ACCOUNT;
import static com.kuma.accounting.message.enums.Process.SHOW_FUNCTION_MENU;
import static com.kuma.accounting.utils.line.BoxUtils.newBody;
import static com.kuma.accounting.utils.line.BoxUtils.newCancelFooter;
import static com.kuma.accounting.utils.line.BoxUtils.newFooter;
import static com.kuma.accounting.utils.line.BoxUtils.newHeader;
import static com.kuma.accounting.utils.line.ButtonUtils.newButton;
import static com.kuma.accounting.utils.line.FlexMessageUtils.newConfirmAndCancelMessage;
import static com.kuma.accounting.utils.line.FlexMessageUtils.newMessage;
import static java.util.Optional.empty;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.accounting.handler.line.AbstractMessageEventHandler;
import com.kuma.accounting.message.enums.Process;
import com.kuma.accounting.model.Account;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.service.AccountService;
import com.kuma.accounting.utils.line.BoxUtils;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;

@Component
public class AccountRemovingHandler extends AbstractMessageEventHandler {

	@Autowired
	private AccountService accountService;

	@Autowired
	private AccountCheckingHandler accountCheckingHandler;

	@Override
	protected Process getSupportedProcess() {
		return REMOVE_ACCOUNT;
	}

	@Override
	protected void handleTextEvent(String replyToken, Member member, String parameter) {
		Optional<Process> processOptional = member.getProcess();
		if (!processOptional.isPresent()) {
			startRemovingAccountProcess(replyToken, member, parameter);
		} else {
			runRemovingAccountProcess(replyToken, member, parameter);
		}
	}

	private void startRemovingAccountProcess(String replyToken, Member member, String parameter) {
		accountCheckingHandler.handleTextEvent(replyToken, member, parameter);
	}

	private void runRemovingAccountProcess(String replyToken, Member member, String parameter) {
		Optional<Account> accountOptional = getAccount(member);
		if (!accountOptional.isPresent()) {
			accountOptional = accountService.findByUserIdAndAccountName(member.getId(), parameter);
			if (accountOptional.isPresent()) {
				Account account = accountOptional.get();
				member.setParameters(parameter);
				memberService.saveMember(member);

				replyConfirmRemovingAccountMessage(replyToken, account);
			} else {
				Box header = newHeader(NO_ACCOUNT_FOUND.getText());
				Box body = newBody(ACCOUNT_NAME_NOT_FOUND.getText());
				Box footer = newCancelFooter();

				FlexMessage message = newMessage(NO_ACCOUNT_FOUND.getText(), header, body, footer);
				messageHelper.reply(replyToken, message);
			}
		} else {
			Account account = accountOptional.get();
			if (parameter.equals(CONFIRM.getText())) {
				removeAccount(replyToken, member, account);
			} else {
				replyConfirmRemovingAccountMessage(replyToken, account);
			}
		}
	}

	private Optional<Account> getAccount(Member member) {
		Optional<String> parametersOptional = member.getParameters();
		if (parametersOptional.isPresent()) {
			String parameters = parametersOptional.get();
			return accountService.findByUserIdAndAccountName(member.getId(), parameters);
		} else {
			return empty();
		}
	}

	private void replyConfirmRemovingAccountMessage(String replyToken, Account account) {
		Box accountInfoBody = BoxUtils.newAccountInfoBody(account);

		FlexMessage message = newConfirmAndCancelMessage(CONFIRM_REMOVE_ACCOUNT.getText(), accountInfoBody);
		messageHelper.reply(replyToken, message);
	}

	private void removeAccount(String replyToken, Member member, Account account) {
		accountService.removeAccount(member, account);
		memberService.resetProcess(member);

		Box header = newHeader(ACCOUNT_REMOVED.getText());
		Box body = newBody(REMOVED.getText() + account.getName());

		Button addCreditCardButton = newButton(ADD_ACCOUNT.getText());
		Button functionMenuButton = newButton(SHOW_FUNCTION_MENU.getText());
		Box footer = newFooter(addCreditCardButton, functionMenuButton);

		FlexMessage message = newMessage(ACCOUNT_REMOVED.getText(), header, body, footer);
		messageHelper.reply(replyToken, message);
	}
}
