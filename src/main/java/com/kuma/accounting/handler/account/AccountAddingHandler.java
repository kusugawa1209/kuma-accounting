package com.kuma.accounting.handler.account;

import static com.kuma.accounting.message.enums.Process.ADD_ACCOUNT;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.accounting.handler.line.AbstractMessageEventHandler;
import com.kuma.accounting.message.enums.Process;
import com.kuma.accounting.model.Member;

@Component
public class AccountAddingHandler extends AbstractMessageEventHandler {

	@Autowired
	private AccountAddingProcessHandler handler;

	@Override
	protected Process getSupportedProcess() {
		return ADD_ACCOUNT;
	}

	@Override
	protected void handleTextEvent(String replyToken, Member member, String parameter) {
		handler.handle(replyToken, member, parameter);
	}
}
