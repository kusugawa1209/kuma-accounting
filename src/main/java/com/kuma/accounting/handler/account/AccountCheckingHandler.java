package com.kuma.accounting.handler.account;

import static com.kuma.accounting.message.enums.Message.NO_ACCOUNT_TEXT;
import static com.kuma.accounting.message.enums.Message.NO_ACCOUNT_TITLE;
import static com.kuma.accounting.message.enums.Process.ADD_ACCOUNT;
import static com.kuma.accounting.message.enums.Process.CHECK_ACCOUNT;
import static com.kuma.accounting.message.enums.Process.REMOVE_ACCOUNT;
import static com.kuma.accounting.message.enums.Process.SHOW_FUNCTION_MENU;
import static com.kuma.accounting.utils.line.BoxUtils.newAccountInfoBody;
import static com.kuma.accounting.utils.line.BoxUtils.newBody;
import static com.kuma.accounting.utils.line.BoxUtils.newFooter;
import static com.kuma.accounting.utils.line.BoxUtils.newHeader;
import static com.kuma.accounting.utils.line.BubbleUtils.newBubble;
import static com.kuma.accounting.utils.line.ButtonUtils.newButton;
import static com.kuma.accounting.utils.line.FlexMessageUtils.newCarouselMessage;
import static com.kuma.accounting.utils.line.FlexMessageUtils.newMessage;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.accounting.handler.line.AbstractMessageEventHandler;
import com.kuma.accounting.message.enums.Process;
import com.kuma.accounting.model.Account;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.service.AccountService;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.container.Bubble;

@Component
public class AccountCheckingHandler extends AbstractMessageEventHandler {

	@Autowired
	private AccountService accountService;

	@Override
	protected Process getSupportedProcess() {
		return CHECK_ACCOUNT;
	}

	@Override
	protected void handleTextEvent(String replyToken, Member member, String parameter) {
		memberService.resetProcess(member);

		FlexMessage message;
		List<Account> accounts = accountService.findByUserId(member.getId());
		if (isNotEmpty(accounts)) {
			message = getAccountsMessage(accounts);
		} else {
			message = getNoAccountMessage();
		}

		messageHelper.reply(replyToken, message);
	}

	private FlexMessage getAccountsMessage(List<Account> accounts) {
		List<Bubble> bubbles = accounts.stream().map(account -> {
			String accountName = account.getName();
			Box header = newHeader(accountName);
			Box body = newAccountInfoBody(account);

			Button removeAccountButton = newButton(REMOVE_ACCOUNT.getText(),
					REMOVE_ACCOUNT.getText() + " " + accountName);
			Button functionMenuButton = newButton(SHOW_FUNCTION_MENU.getText());
			Box footer = newFooter(removeAccountButton, functionMenuButton);

			return newBubble(header, body, footer);
		}).collect(toList());

		return newCarouselMessage(CHECK_ACCOUNT.getText(), bubbles);
	}

	private FlexMessage getNoAccountMessage() {
		Box header = newHeader(NO_ACCOUNT_TITLE.getText());
		Box body = newBody(NO_ACCOUNT_TEXT.getText());

		Button checkCreditCardButton = newButton(ADD_ACCOUNT.getText());
		Button functionMenuButton = newButton(SHOW_FUNCTION_MENU.getText());
		Box footer = newFooter(checkCreditCardButton, functionMenuButton);

		return newMessage(NO_ACCOUNT_TITLE.getText(), header, body, footer);
	}
}
