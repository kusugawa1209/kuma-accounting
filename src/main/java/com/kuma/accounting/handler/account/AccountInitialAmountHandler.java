package com.kuma.accounting.handler.account;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.accounting.dto.AccountParameter;
import com.kuma.accounting.handler.process.AbstractHandler;
import com.kuma.accounting.handler.process.ProcessHandler;
import com.kuma.accounting.message.enums.AccountMessage;
import com.kuma.accounting.message.enums.Message;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.utils.line.BoxUtils;
import com.kuma.accounting.utils.line.FlexMessageUtils;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;

@Component
public class AccountInitialAmountHandler extends AbstractHandler {

	@Autowired
	private AccountConfirmHandler handler;

	@Override
	protected boolean shouldHandle(Member member, String parameter) {
		AccountParameter parameters = member.getParameter(AccountParameter.class);

		return Objects.isNull(parameters.getInitialAmount());
	}

	@Override
	protected void tryHandle(String replyToken, Member member, String parameter) {
		AccountParameter parameters = member.getParameter(AccountParameter.class);

		try {
			BigDecimal initialAmount = new BigDecimal(parameter);
			parameters.setInitialAmount(initialAmount);

			member.setParameter(parameters);
			memberService.saveMember(member);

			Box accountInfoBody = BoxUtils.newAccountInfoBody(parameters);
			FlexMessage message = FlexMessageUtils.newConfirmAndCancelMessage(Message.CONFIRM_ADD_ACCOUNT.getText(),
					accountInfoBody);
			messageHelper.reply(replyToken, message);
		} catch (NumberFormatException e) {
			AccountMessage currentStep = AccountMessage.INITIAL_AMOUNT;

			FlexMessage message = FlexMessageUtils.newMessageWithCancelFooter(currentStep.getTitle(),
					currentStep.getMessage());
			messageHelper.reply(replyToken, message);
		}
	}

	@Override
	protected Optional<ProcessHandler> getNextHandler(Member member) {
		return Optional.of(handler);
	}

}
