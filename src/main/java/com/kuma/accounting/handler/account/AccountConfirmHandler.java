package com.kuma.accounting.handler.account;

import java.math.BigDecimal;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.accounting.dto.AccountParameter;
import com.kuma.accounting.handler.process.AbstractHandler;
import com.kuma.accounting.handler.process.ProcessHandler;
import com.kuma.accounting.message.enums.Message;
import com.kuma.accounting.message.enums.Process;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.utils.line.BoxUtils;
import com.kuma.accounting.utils.line.ButtonUtils;
import com.kuma.accounting.utils.line.FlexMessageUtils;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;

@Component
public class AccountConfirmHandler extends AbstractHandler {

	@Autowired
	private AccountReConfirmHandler handler;

	@Override
	protected boolean shouldHandle(Member member, String parameter) {
		return parameter.equals(Message.CONFIRM.getText());
	}

	@Override
	protected void tryHandle(String replyToken, Member member, String parameter) {
		AccountParameter parameters = member.getParameter(AccountParameter.class);

		String accountName = parameters.getAccountName();
		BigDecimal initialAmount = parameters.getInitialAmount();

		accountService.createAccount(member, accountName, initialAmount);
		memberService.resetProcess(member);

		Box header = BoxUtils.newHeader(Message.ACCOUNT_SAVED.getText());
		Box body = BoxUtils.newAccountInfoBody(parameters);

		Button addAccountButton = ButtonUtils.newButton(Process.ADD_ACCOUNT.getText());
		Button functionMenuButton = ButtonUtils.newButton(Process.SHOW_FUNCTION_MENU.getText());
		Box footer = BoxUtils.newFooter(addAccountButton, functionMenuButton);

		FlexMessage message = FlexMessageUtils.newMessage(Message.ACCOUNT_SAVED.getText(), header, body, footer);
		messageHelper.reply(replyToken, message);
	}

	@Override
	protected Optional<ProcessHandler> getNextHandler(Member member) {
		return Optional.of(handler);
	}
}
