package com.kuma.accounting.handler.account;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.accounting.dto.AccountParameter;
import com.kuma.accounting.handler.process.AbstractHandler;
import com.kuma.accounting.handler.process.ProcessHandler;
import com.kuma.accounting.message.enums.AccountMessage;
import com.kuma.accounting.message.enums.Message;
import com.kuma.accounting.model.Account;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.utils.line.FlexMessageUtils;
import com.linecorp.bot.model.message.FlexMessage;

@Component
public class AccountNameHandler extends AbstractHandler {

	@Autowired
	private AccountInitialAmountHandler handler;

	@Override
	protected boolean shouldHandle(Member member, String parameter) {
		AccountParameter parameters = member.getParameter(AccountParameter.class);
		return StringUtils.isBlank(parameters.getAccountName());
	}

	@Override
	protected void tryHandle(String replyToken, Member member, String parameter) {
		Optional<Account> existedAccountOptional = accountService.findByUserIdAndAccountName(member.getId(), parameter);
		if (existedAccountOptional.isPresent()) {
			FlexMessage message = FlexMessageUtils.newMessageWithCancelFooter(Message.ACCOUNT_NAME_EXISTS.getText(),
					AccountMessage.ACCOUNT_NAME.getMessage());

			messageHelper.reply(replyToken, message);
		} else {
			AccountParameter parameters = member.getParameter(AccountParameter.class);
			parameters.setAccountName(parameter);

			member.setParameter(parameters);
			memberService.saveMember(member);
			
			AccountMessage nextStep = AccountMessage.INITIAL_AMOUNT;

			FlexMessage message = FlexMessageUtils.newMessageWithCancelFooter(nextStep.getTitle(),
					nextStep.getMessage());
			messageHelper.reply(replyToken, message);
		}
	}

	@Override
	protected Optional<ProcessHandler> getNextHandler(Member member) {
		return Optional.of(handler);
	}

}
