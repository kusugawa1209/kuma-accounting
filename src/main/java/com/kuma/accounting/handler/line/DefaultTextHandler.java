package com.kuma.accounting.handler.line;

import static com.kuma.accounting.message.enums.Message.WELCOME;
import static com.kuma.accounting.utils.line.BoxUtils.newFunctionMenu;
import static com.kuma.accounting.utils.line.FlexMessageUtils.newMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.accounting.helper.EventHelper;
import com.kuma.accounting.helper.MessageHelper;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;

@Component
public class DefaultTextHandler {

	@Autowired
	private EventHelper eventHelper;

	@Autowired
	private MessageHelper messageHelper;

	public void handleTextEvent(MessageEvent<TextMessageContent> event) {
		Box body = newFunctionMenu();

		FlexMessage message = newMessage(WELCOME.getText(), body);

		String replyToken = eventHelper.getReplyToken(event);
		messageHelper.reply(replyToken, message);
	}
}
