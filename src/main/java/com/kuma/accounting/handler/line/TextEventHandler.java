package com.kuma.accounting.handler.line;

import static com.kuma.accounting.message.enums.Process.CANCEL;
import static com.kuma.accounting.message.enums.Process.REMOVE_ACCOUNT;
import static com.kuma.accounting.message.enums.Process.getTextMessageKeywordsByText;
import static java.util.Objects.nonNull;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;
import static org.apache.commons.lang3.StringUtils.removeStart;
import static org.apache.commons.lang3.StringUtils.startsWith;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.kuma.accounting.handler.process.CancelHandler;
import com.kuma.accounting.helper.EventHelper;
import com.kuma.accounting.helper.MessageHelper;
import com.kuma.accounting.message.enums.Process;
import com.kuma.accounting.model.Account;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.service.AccountService;
import com.kuma.accounting.service.MemberService;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;

@LineMessageHandler
public class TextEventHandler {

	@Autowired
	private EventHelper eventHelper;

	@Autowired
	private MessageHelper messageHelper;

	@Autowired
	private DefaultTextHandler defaultTextHandler;

	@Autowired
	private CancelHandler cancelHandler;

	@Autowired
	private ApplicationContext applicationContext;

	@Autowired
	private MemberService memberService;

	@Autowired
	private AccountService accountService;
	
	protected Map<Process, AbstractMessageEventHandler> handlers;

	private static final String REMOVE_ACCOUNT_TEXT_PREFIX = REMOVE_ACCOUNT.getText() + " ";

	@PostConstruct
	public void init() {
		this.handlers = applicationContext.getBeansOfType(AbstractMessageEventHandler.class).entrySet().stream()
				.map(Entry<String, AbstractMessageEventHandler>::getValue)
				.collect(toMap(AbstractMessageEventHandler::getSupportedProcess, identity()));
	}

	@EventMapping
	public void handleTextMessageEvent(MessageEvent<TextMessageContent> event) {
		String userId = eventHelper.getUserId(event);
		Optional<Member> memberOptional = memberService.findMember(userId);

		memberOptional.ifPresent(member -> {
			String text = event.getMessage().getText();
			if (CANCEL.getText().equals(text)) {
				cancelHandler.handleTextEvent(event);
			} else {
				if (startsWith(text, REMOVE_ACCOUNT_TEXT_PREFIX)) {
					String accountName = removeStart(text, REMOVE_ACCOUNT_TEXT_PREFIX);

					Optional<Account> accountOptional = accountService.findByUserIdAndAccountName(userId, accountName);
					if (accountOptional.isPresent()) {
						member.setProcess(REMOVE_ACCOUNT);
						member.setParameters(accountName);

						memberService.saveMember(member);

						AbstractMessageEventHandler handler = handlers.get(REMOVE_ACCOUNT);
						String replyToken = eventHelper.getReplyToken(event);
						handler.handleTextEvent(replyToken, member, accountName);
					} else {
						handleDefaultText(event, member);
					}
				} else {
					handleDefaultText(event, member);
				}
			}
		});
	}

	private void handleDefaultText(MessageEvent<TextMessageContent> event, Member member) {
		Optional<Process> processOptional = member.getProcess();
		if (processOptional.isPresent()) {
			Process process = processOptional.get();
			handleMemberWithConversationProcess(event, process);
		} else {
			handleMemberWithoutConversationProcess(event);
		}
	}

	private void handleMemberWithConversationProcess(MessageEvent<TextMessageContent> event, Process process) {
		String replyToken = eventHelper.getReplyToken(event);

		AbstractMessageEventHandler handler = handlers.get(process);
		if (nonNull(handler)) {
			handler.handleTextEvent(event);
		} else {
			messageHelper.replyError(replyToken);
		}
	}

	private void handleMemberWithoutConversationProcess(MessageEvent<TextMessageContent> event) {
		String messageText = event.getMessage().getText();
		Optional<Process> processOptional = getTextMessageKeywordsByText(messageText);
		if (processOptional.isPresent()) {
			Process process = processOptional.get();

			AbstractMessageEventHandler handler = handlers.get(process);
			if (nonNull(handler)) {
				handler.handleTextEvent(event);
			} else {
				String replyToken = eventHelper.getReplyToken(event);
				messageHelper.replyError(replyToken);
			}
		} else {
			defaultTextHandler.handleTextEvent(event);
		}
	}

}
