package com.kuma.accounting.handler.line;

import java.util.Optional;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.kuma.accounting.helper.EventHelper;
import com.kuma.accounting.helper.MessageHelper;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.service.MemberService;
import com.kuma.utils.LogUtils;
import com.linecorp.bot.model.event.FollowEvent;
import com.linecorp.bot.model.event.UnfollowEvent;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;

@LineMessageHandler
public class FollowerEventHandler {

	@Autowired
	private MemberService memberService;

	@Autowired
	private EventHelper eventHelper;

	@Autowired
	private MessageHelper messageHelper;

	@EventMapping
	public void handleFollowEvent(FollowEvent event) {
		String userId = eventHelper.getUserId(event);
		String replyToken = eventHelper.getReplyToken(event);
		try {
			String username = eventHelper.getUsername(event);

			Optional<Member> memberOptional = memberService.findMember(userId);
			if (memberOptional.isPresent()) {
				Member member = memberOptional.get();

				if (BooleanUtils.isFalse(member.getIsActive())) {
					member.setIsActive(true);
					member.setName(username);
					memberService.saveMember(member);

					messageHelper.reply(replyToken, username + "，歡迎您回來！");
				}
			} else {
				memberService.createMember(userId, username);
				messageHelper.reply(replyToken, "哈囉～" + username + "，已經幫您新增帳號囉！");
			}
		} catch (ExecutionException e) {
			LogUtils.logThrowable(e);

			messageHelper.replyError(replyToken);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();

			LogUtils.logThrowable(e);

			messageHelper.replyError(replyToken);
		}
	}

	@EventMapping
	public void handleUnfollowEvent(UnfollowEvent event) {
		String userId = eventHelper.getUserId(event);
		Optional<Member> memberOptional = memberService.findMember(userId);
		memberOptional.ifPresent(member -> {
			member.setIsActive(false);

			memberService.saveMember(member);
		});
	}
}
