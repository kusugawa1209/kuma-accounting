package com.kuma.accounting.handler.line;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.accounting.helper.EventHelper;
import com.kuma.accounting.helper.MessageHelper;
import com.kuma.accounting.message.enums.Process;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.service.MemberService;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;

@Component
public abstract class AbstractMessageEventHandler {

	@Autowired
	protected EventHelper eventHelper;

	@Autowired
	protected MessageHelper messageHelper;

	@Autowired
	protected MemberService memberService;
	
	protected abstract Process getSupportedProcess();

	protected abstract void handleTextEvent(String replyToken, Member member, String parameter);

	public void handleTextEvent(MessageEvent<TextMessageContent> event) {
		String userId = eventHelper.getUserId(event);
		Optional<Member> memberOptional = memberService.findMember(userId);

		memberOptional.ifPresent(member -> {
			String replyToken = eventHelper.getReplyToken(event);
			String parameter = event.getMessage().getText();
			handleTextEvent(replyToken, member, parameter);
		});
	}
}
