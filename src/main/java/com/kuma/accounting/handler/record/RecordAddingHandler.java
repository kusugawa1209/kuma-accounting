package com.kuma.accounting.handler.record;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.accounting.handler.line.AbstractMessageEventHandler;
import com.kuma.accounting.message.enums.Process;
import com.kuma.accounting.model.Member;

@Component
public class RecordAddingHandler extends AbstractMessageEventHandler {

	@Autowired
	private RecordAddingProcessHandler handler;

	@Override
	protected Process getSupportedProcess() {
		return Process.ADD_RECORD;
	}

	@Override
	protected void handleTextEvent(String replyToken, Member member, String parameter) {
		handler.handle(replyToken, member, parameter);
	}
}
