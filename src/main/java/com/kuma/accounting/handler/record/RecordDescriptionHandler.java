package com.kuma.accounting.handler.record;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.accounting.dto.TransferRecordParameter;
import com.kuma.accounting.handler.process.AbstractHandler;
import com.kuma.accounting.handler.process.ProcessHandler;
import com.kuma.accounting.message.enums.RecordMessage;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.utils.line.FlexMessageUtils;
import com.linecorp.bot.model.message.FlexMessage;

@Component
public class RecordDescriptionHandler extends AbstractHandler {

	@Autowired
	private RecordAmountHandler handler;

	@Override
	protected boolean shouldHandle(Member member, String parameter) {
		TransferRecordParameter parameters = member.getParameter(TransferRecordParameter.class);

		return StringUtils.isBlank(parameters.getDescription());
	}

	@Override
	protected void tryHandle(String replyToken, Member member, String parameter) {
		TransferRecordParameter parameters = member.getParameter(TransferRecordParameter.class);

		parameters.setDescription(parameter);

		member.setParameter(parameters);
		memberService.saveMember(member);

		RecordMessage nextStep = RecordMessage.INPUT_AMOUNT;
		FlexMessage message = FlexMessageUtils.newMessageWithCancelFooter(nextStep.getTitle(), nextStep.getMessage());
		messageHelper.reply(replyToken, message);
	}

	@Override
	protected Optional<ProcessHandler> getNextHandler(Member member) {
		return Optional.of(handler);
	}

}
