package com.kuma.accounting.handler.record;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.accounting.dto.NormalRecordParameter;
import com.kuma.accounting.handler.process.AbstractHandler;
import com.kuma.accounting.handler.process.ProcessHandler;
import com.kuma.accounting.helper.RecordMessageHelper;
import com.kuma.accounting.message.enums.Format;
import com.kuma.accounting.message.enums.Message;
import com.kuma.accounting.message.enums.RecordMessage;
import com.kuma.accounting.message.enums.RecordType;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.utils.line.FlexMessageUtils;
import com.linecorp.bot.model.message.FlexMessage;

@Component
public class RecordDateHandler extends AbstractHandler {

	@Autowired
	private RecordAccountHandler handler;

	@Autowired
	private RecordMessageHelper recordMessageHelper;

	@Override
	protected boolean shouldHandle(Member member, String parameter) {
		NormalRecordParameter parameters = member.getParameter(NormalRecordParameter.class);

		return Objects.isNull(parameters.getDate());
	}

	@Override
	protected void tryHandle(String replyToken, Member member, String parameter) {
		NormalRecordParameter parameters = member.getParameter(NormalRecordParameter.class);

		try {
			Date date = parseDate(parameter);
			parameters.setDate(date);

			member.setParameter(parameters);
			memberService.saveMember(member);

			RecordMessage message;
			if (RecordType.TRANSFER.equals(parameters.getType())) {
				message = RecordMessage.CHOOSE_SOURCE_ACCOUNT;
			} else {
				message = RecordMessage.CHOOSE_ACCOUNT;
			}
			recordMessageHelper.replyChooseAccountMessage(member, replyToken, message);

		} catch (ParseException e) {
			RecordMessage currentStep = RecordMessage.CHOOSE_DATE;

			FlexMessage message = FlexMessageUtils.newMessageWithCancelFooter(currentStep.getTitle(),
					currentStep.getMessage());
			messageHelper.reply(replyToken, message);
		}
	}

	private Date parseDate(String parameter) throws ParseException {
		Calendar calendar = Calendar.getInstance();
		if (StringUtils.equalsIgnoreCase(Message.YESTERDAY.getText(), parameter)) {
			calendar.add(Calendar.DATE, -1);
			return calendar.getTime();
		} else if (StringUtils.equalsIgnoreCase(Message.TODAY.getText(), parameter)) {
			return calendar.getTime();
		} else {
			return new SimpleDateFormat(Format.RECORD_DATE_FORMAT.getStringFormat()).parse(parameter);
		}
	}

	@Override
	protected Optional<ProcessHandler> getNextHandler(Member member) {
		return Optional.of(handler);
	}

}
