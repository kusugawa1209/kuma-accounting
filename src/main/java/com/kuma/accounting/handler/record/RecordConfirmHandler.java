package com.kuma.accounting.handler.record;

import java.util.Date;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.accounting.dto.DayStatistics;
import com.kuma.accounting.dto.MonthStatistics;
import com.kuma.accounting.dto.NormalRecordParameter;
import com.kuma.accounting.dto.TransferRecordParameter;
import com.kuma.accounting.handler.process.AbstractHandler;
import com.kuma.accounting.handler.process.ProcessHandler;
import com.kuma.accounting.info.AccountInfo;
import com.kuma.accounting.message.enums.AccountType;
import com.kuma.accounting.message.enums.Message;
import com.kuma.accounting.message.enums.Process;
import com.kuma.accounting.message.enums.RecordType;
import com.kuma.accounting.model.BaseAccount;
import com.kuma.accounting.model.BaseRecord;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.model.NormalRecord;
import com.kuma.accounting.model.TransferRecord;
import com.kuma.accounting.utils.line.BoxUtils;
import com.kuma.accounting.utils.line.BubbleUtils;
import com.kuma.accounting.utils.line.ButtonUtils;
import com.kuma.accounting.utils.line.FlexMessageUtils;
import com.kuma.accounting.utils.line.SeparatorUtils;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.container.Bubble;

@Component
@Transactional
public class RecordConfirmHandler extends AbstractHandler {

	@Autowired
	private RecordReConfirmHandler handler;

	@Override
	protected boolean shouldHandle(Member member, String parameter) {
		return parameter.equals(Message.CONFIRM.getText());
	}

	@Override
	protected void tryHandle(String replyToken, Member member, String parameter) {
		TransferRecordParameter parameters = member.getParameter(TransferRecordParameter.class);
		RecordType recordType = parameters.getType();

		String userId = member.getId();
		if (RecordType.TRANSFER.equals(recordType)) {
			Optional<? extends BaseAccount> accountOptional = getAccount(userId, parameters);
			Optional<? extends BaseAccount> targetAccountOptional = getTargetAccount(userId, parameters);

			if (accountOptional.isPresent() && targetAccountOptional.isPresent()) {

				BaseAccount sourceAccount = accountOptional.get();
				BaseAccount targetAccount = targetAccountOptional.get();

				TransferRecord record = transferRecordService.createRecord(member, sourceAccount, targetAccount,
						parameters.getDate(), parameters.getDescription(), parameters.getAmount());
				memberService.resetProcess(member);

				replyTransferRecordSavedMessage(replyToken, member, record);
			}
		} else {
			Optional<? extends BaseAccount> accountOptional = getAccount(userId, parameters);
			if (accountOptional.isPresent()) {
				BaseAccount account = accountOptional.get();

				NormalRecord record = normalRecordService.createRecord(member, account, recordType,
						parameters.getDate(), parameters.getDescription(), parameters.getAmount());

				memberService.resetProcess(member);

				replyNormalRecordSavedMessage(replyToken, member, record);
			}
		}
	}

	@Override
	protected Optional<ProcessHandler> getNextHandler(Member member) {
		return Optional.of(handler);
	}

	private void replyNormalRecordSavedMessage(String replyToken, Member member, NormalRecord record) {
		String userId = member.getId();
		RecordType recordType = record.getType();
		String altText = recordType.getText() + Message.RECORD_SAVED.getText();

		Box header = BoxUtils.newHeader(altText);

		Button addRecordButton = ButtonUtils.newButton(Process.ADD_RECORD.getText());
		Button functionMenuButton = ButtonUtils.newButton(Process.SHOW_FUNCTION_MENU.getText());
		Box footer = BoxUtils.newFooter(addRecordButton, functionMenuButton);

		AccountInfo accountInfo = getAccountInfo(userId, record.getAccount());
		Bubble recordBubble = BubbleUtils.newRecordInfoBubble(header, record, accountInfo, footer);

		Bubble statisticBubble = getStatisticBubble(member, record);

		FlexMessage message = FlexMessageUtils.newCarouselMessage(altText, recordBubble, statisticBubble);
		messageHelper.reply(replyToken, message);
	}

	private void replyTransferRecordSavedMessage(String replyToken, Member member, TransferRecord record) {
		String userId = member.getId();
		RecordType recordType = record.getType();
		String altText = recordType.getText() + Message.RECORD_SAVED.getText();

		Box header = BoxUtils.newHeader(altText);

		AccountInfo sourceAccountInfo = getAccountInfo(userId, record.getSourceAccount());
		AccountInfo targetAccountInfo = getAccountInfo(userId, record.getTargetAccount());
		Bubble recordBubble = getTransferRecordBubble(header, record, sourceAccountInfo, targetAccountInfo);

		Bubble statisticBubble = getStatisticBubble(member, record);

		FlexMessage message = FlexMessageUtils.newCarouselMessage(altText, recordBubble, statisticBubble);
		messageHelper.reply(replyToken, message);
	}

	private Bubble getTransferRecordBubble(Box header, TransferRecord record, AccountInfo sourceAccountInfo,
			AccountInfo targetAccountInfo) {
		Box recordBox = BoxUtils.newTransferRecordInfoBody(record);

		Box sourceAccountInfoBox = BoxUtils.newAccountInfoBody(sourceAccountInfo);

		Box targetAccountInfoBox = BoxUtils.newAccountInfoBody(targetAccountInfo);

		Separator separator = SeparatorUtils.newSeparator();

		Box body = BoxUtils.newBody(recordBox, separator, sourceAccountInfoBox, separator, targetAccountInfoBox);

		Button addRecordButton = ButtonUtils.newButton(Process.ADD_RECORD.getText());
		Button functionMenuButton = ButtonUtils.newButton(Process.SHOW_FUNCTION_MENU.getText());
		Box footer = BoxUtils.newFooter(addRecordButton, functionMenuButton);

		return BubbleUtils.newBubble(header, body, footer);
	}

	private Bubble getStatisticBubble(Member member, BaseRecord record) {
		Date date = record.getDate();

		DayStatistics dayStatistics = statisticsService.getDayStatistics(member, date);
		Box dayStatisticsBox = BoxUtils.newDayStatisticBody(dayStatistics);

		Separator separator = SeparatorUtils.newSeparator();

		MonthStatistics monthStatistics = statisticsService.getMonthStatistics(member, date);
		Box monthStatisticsBox = BoxUtils.newMonthStatisticBody(monthStatistics);

		Box body = BoxUtils.newBody(dayStatisticsBox, separator, monthStatisticsBox);

		Box header = BoxUtils.newHeader(Message.STATISTICS_INFO.getText());

		return BubbleUtils.newBubble(header, body);
	}

	private AccountInfo getAccountInfo(String userId, BaseAccount account) {
		AccountInfo accountInfo;
		AccountType baseAccountType = account.getType();
		Long baseAccountId = account.getId();
		if (AccountType.ACCOUNT.equals(baseAccountType)) {
			accountInfo = accountService.getByUserIdAndAccountId(userId, baseAccountId);
		} else {
			accountInfo = creditCardService.getByUserIdAndCreditCardId(userId, baseAccountId);
		}
		return accountInfo;
	}

	private Optional<? extends BaseAccount> getAccount(String userId, NormalRecordParameter parameters) {
		AccountType accountType = parameters.getAccountType();
		String accountName = parameters.getAccountName();

		if (AccountType.ACCOUNT.equals(accountType)) {
			return accountService.findByUserIdAndAccountName(userId, accountName);
		} else if (AccountType.CREDIT_CARD.equals(accountType)) {
			return creditCardService.findByUserIdAndCreditCardName(userId, accountName);
		} else {
			return Optional.empty();
		}
	}

	private Optional<? extends BaseAccount> getTargetAccount(String userId, TransferRecordParameter parameters) {
		AccountType accountType = parameters.getTargetAccountType();
		String accountName = parameters.getTargetAccountName();

		if (AccountType.ACCOUNT.equals(accountType)) {
			return accountService.findByUserIdAndAccountName(userId, accountName);
		} else if (AccountType.CREDIT_CARD.equals(accountType)) {
			return creditCardService.findByUserIdAndCreditCardName(userId, accountName);
		} else {
			return Optional.empty();
		}
	}
}
