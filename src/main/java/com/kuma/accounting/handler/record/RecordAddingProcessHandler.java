package com.kuma.accounting.handler.record;

import static com.kuma.accounting.message.enums.Process.SHOW_FUNCTION_MENU;
import static com.kuma.accounting.utils.line.BoxUtils.newFooter;
import static com.kuma.accounting.utils.line.BoxUtils.newHeader;
import static com.kuma.accounting.utils.line.ButtonUtils.newButton;
import static com.kuma.accounting.utils.line.FlexMessageUtils.newMessage;

import java.util.List;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.accounting.handler.process.AbstractHandler;
import com.kuma.accounting.handler.process.ProcessHandler;
import com.kuma.accounting.helper.RecordMessageHelper;
import com.kuma.accounting.message.enums.Message;
import com.kuma.accounting.message.enums.Process;
import com.kuma.accounting.model.Account;
import com.kuma.accounting.model.CreditCard;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.utils.line.BoxUtils;
import com.kuma.accounting.utils.line.ButtonUtils;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;

@Component
public class RecordAddingProcessHandler extends AbstractHandler {

	@Autowired
	private RecordTypeHandler handler;

	@Autowired
	private RecordMessageHelper recordMessageHelper;

	@Override
	protected boolean shouldHandle(Member member, String parameter) {
		Optional<Process> processOptional = member.getProcess();

		return !processOptional.isPresent();
	}

	@Override
	protected void tryHandle(String replyToken, Member member, String parameter) {
		List<Account> accounts = accountService.findByUserId(member.getId());
		List<CreditCard> creditCards = creditCardService.findByUserId(member.getId());
		if (CollectionUtils.isNotEmpty(accounts) || CollectionUtils.isNotEmpty(creditCards)) {
			member.setProcess(Process.ADD_RECORD);
			memberService.saveMember(member);

			recordMessageHelper.replyChooseTypeMessage(replyToken);
		} else {
			Box header = newHeader(Message.ADD_ACCOUNT_OR_CREDIT_CARD_BEFORE_ADDING_RECORD.getText());

			Button addAccountButton = ButtonUtils.newButton(Process.ADD_ACCOUNT.getText());
			Button addCreditCardButton = ButtonUtils.newButton(Process.ADD_CREDIT_CARD.getText());
			Box body = BoxUtils.newBody(addAccountButton, addCreditCardButton);

			Button cancelButton = newButton(Process.CANCEL.getText());
			Button functionMenuButton = newButton(SHOW_FUNCTION_MENU.getText());
			Box footer = newFooter(cancelButton, functionMenuButton);

			FlexMessage message = newMessage(Message.ADD_ACCOUNT_OR_CREDIT_CARD_BEFORE_ADDING_RECORD.getText(), header,
					body, footer);
			messageHelper.reply(replyToken, message);
		}

	}

	@Override
	protected Optional<ProcessHandler> getNextHandler(Member member) {
		return Optional.of(handler);
	}

}
