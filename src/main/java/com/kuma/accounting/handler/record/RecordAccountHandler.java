package com.kuma.accounting.handler.record;

import static com.kuma.accounting.message.enums.AccountType.ACCOUNT;
import static com.kuma.accounting.message.enums.AccountType.CREDIT_CARD;

import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.accounting.dto.NormalRecordParameter;
import com.kuma.accounting.handler.process.AbstractHandler;
import com.kuma.accounting.handler.process.ProcessHandler;
import com.kuma.accounting.helper.RecordMessageHelper;
import com.kuma.accounting.message.enums.AccountType;
import com.kuma.accounting.message.enums.RecordMessage;
import com.kuma.accounting.message.enums.RecordType;
import com.kuma.accounting.model.BaseAccount;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.utils.line.FlexMessageUtils;
import com.linecorp.bot.model.message.FlexMessage;

@Component
public class RecordAccountHandler extends AbstractHandler {

	@Autowired
	private RecordDescriptionHandler handler;

	@Autowired
	private TransferRecordTargetAccountHandler transferRecordHandler;

	@Autowired
	private RecordMessageHelper recordMessageHelper;

	private static final Pattern ACCOUNT_PATTERN = Pattern
			.compile("(" + ACCOUNT.getName() + "|" + CREDIT_CARD.getName() + ") (.+)");

	@Override
	protected boolean shouldHandle(Member member, String parameter) {
		NormalRecordParameter parameters = member.getParameter(NormalRecordParameter.class);

		return StringUtils.isBlank(parameters.getAccountName()) || Objects.isNull(parameters.getAccountType());
	}

	@Override
	protected void tryHandle(String replyToken, Member member, String parameter) {
		NormalRecordParameter parameters = member.getParameter(NormalRecordParameter.class);

		Optional<? extends BaseAccount> accountOptional = getAccount(member.getId(), parameter);
		if (accountOptional.isPresent()) {

			BaseAccount account = accountOptional.get();

			AccountType accountType = account.getType();
			parameters.setAccountType(accountType);

			String accountName = account.getName();
			parameters.setAccountName(accountName);

			member.setParameter(parameters);
			memberService.saveMember(member);

			RecordType recordType = parameters.getType();
			if (RecordType.TRANSFER.equals(recordType)) {
				recordMessageHelper.replyChooseAccountMessage(member, replyToken, RecordMessage.CHOOSE_TARGET_ACCOUNT);
			} else {
				RecordMessage nextStep = RecordMessage.INPUT_DESCRIPTION;

				FlexMessage message = FlexMessageUtils.newMessageWithCancelFooter(nextStep.getTitle(),
						nextStep.getMessage());
				messageHelper.reply(replyToken, message);
			}

		} else {
			recordMessageHelper.replyChooseAccountMessage(member, replyToken, RecordMessage.CHOOSE_SOURCE_ACCOUNT);
		}
	}

	@Override
	protected Optional<ProcessHandler> getNextHandler(Member member) {
		NormalRecordParameter parameters = member.getParameter(NormalRecordParameter.class);

		RecordType recordType = parameters.getType();
		if (RecordType.TRANSFER.equals(recordType)) {
			return Optional.of(transferRecordHandler);
		} else {
			return Optional.of(handler);
		}
	}

	private Optional<? extends BaseAccount> getAccount(String userId, String parameter) {
		if (StringUtils.isBlank(parameter)) {
			return Optional.empty();
		}

		Matcher matcher = ACCOUNT_PATTERN.matcher(parameter);
		if (!matcher.find()) {
			return Optional.empty();
		}

		String accountTypeName = matcher.group(1);
		Optional<AccountType> accountTypeOptional = AccountType.getByName(accountTypeName);
		if (!accountTypeOptional.isPresent()) {
			return Optional.empty();
		}
		AccountType accountType = accountTypeOptional.get();
		String accountName = matcher.group(2);
		if (AccountType.ACCOUNT.equals(accountType)) {
			return accountService.findByUserIdAndAccountName(userId, accountName);
		} else if (AccountType.CREDIT_CARD.equals(accountType)) {
			return creditCardService.findByUserIdAndCreditCardName(userId, accountName);
		} else {
			return Optional.empty();
		}
	}
}
