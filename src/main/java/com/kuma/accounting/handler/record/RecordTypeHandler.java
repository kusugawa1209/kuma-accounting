package com.kuma.accounting.handler.record;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.accounting.dto.NormalRecordParameter;
import com.kuma.accounting.handler.process.AbstractHandler;
import com.kuma.accounting.handler.process.ProcessHandler;
import com.kuma.accounting.helper.RecordMessageHelper;
import com.kuma.accounting.message.enums.Format;
import com.kuma.accounting.message.enums.Message;
import com.kuma.accounting.message.enums.RecordMessage;
import com.kuma.accounting.message.enums.RecordType;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.utils.line.BoxUtils;
import com.kuma.accounting.utils.line.ButtonUtils;
import com.kuma.accounting.utils.line.FlexMessageUtils;
import com.kuma.accounting.utils.line.TextUtils;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Text;

@Component
public class RecordTypeHandler extends AbstractHandler {

	@Autowired
	private RecordMessageHelper recordMessageHelper;

	@Autowired
	private RecordDateHandler handler;

	@Override
	protected boolean shouldHandle(Member member, String parameter) {
		NormalRecordParameter parameters = member.getParameter(NormalRecordParameter.class);

		return Objects.isNull(parameters.getType());
	}

	@Override
	protected void tryHandle(String replyToken, Member member, String parameter) {
		Optional<RecordType> recordTypeOptional = RecordType.getRecordTypeByText(parameter);
		if (recordTypeOptional.isPresent()) {
			RecordType recordType = recordTypeOptional.get();

			NormalRecordParameter parameters = member.getParameter(NormalRecordParameter.class);
			parameters.setType(recordType);

			member.setParameter(parameters);
			memberService.saveMember(member);

			String altText = RecordMessage.CHOOSE_DATE.getTitle();

			String chooseDateMessage = RecordMessage.CHOOSE_DATE.getMessage();
			Text chooseDateText = TextUtils.newBodyText(chooseDateMessage);

			SimpleDateFormat dateFormat = new SimpleDateFormat(Format.RECORD_DATE_FORMAT.getStringFormat());

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());

			String today = dateFormat.format(calendar.getTime());

			calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) - 1);
			String yesterday = dateFormat.format(calendar.getTime());

			Button yesterdayButton = ButtonUtils.newButton(Message.YESTERDAY.getText(), yesterday);
			Button todayButton = ButtonUtils.newButton(Message.TODAY.getText(), today);
			
			Box body = BoxUtils.newBody(chooseDateText, yesterdayButton, todayButton);

			FlexMessage message = FlexMessageUtils.newMessageWithCancelFooter(altText, body);
			messageHelper.reply(replyToken, message);
		} else {
			recordMessageHelper.replyChooseTypeMessage(replyToken);
		}
	}

	@Override
	protected Optional<ProcessHandler> getNextHandler(Member member) {
		return Optional.of(handler);
	}

}
