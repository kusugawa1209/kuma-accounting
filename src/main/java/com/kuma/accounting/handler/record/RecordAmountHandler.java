package com.kuma.accounting.handler.record;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.accounting.dto.TransferRecordParameter;
import com.kuma.accounting.handler.process.AbstractHandler;
import com.kuma.accounting.handler.process.ProcessHandler;
import com.kuma.accounting.message.enums.RecordMessage;
import com.kuma.accounting.message.enums.RecordType;
import com.kuma.accounting.message.enums.TextTemplate;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.utils.line.BoxUtils;
import com.kuma.accounting.utils.line.FlexMessageUtils;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;

@Component
public class RecordAmountHandler extends AbstractHandler {

	@Autowired
	private RecordConfirmHandler handler;

	@Override
	protected boolean shouldHandle(Member member, String parameter) {
		TransferRecordParameter parameters = member.getParameter(TransferRecordParameter.class);

		return Objects.isNull(parameters.getAmount());
	}

	@Override
	protected void tryHandle(String replyToken, Member member, String parameter) {
		TransferRecordParameter parameters = member.getParameter(TransferRecordParameter.class);

		try {
			BigDecimal amount = new BigDecimal(parameter);
			parameters.setAmount(amount);

			member.setParameter(parameters);
			memberService.saveMember(member);

			Box recordBody;
			RecordType recordType = parameters.getType();
			if (RecordType.TRANSFER.equals(recordType)) {
				recordBody = BoxUtils.newTransferRecordInfoBody(parameters);
			} else {
				recordBody = BoxUtils.newNormalRecordInfoBody(parameters);
			}

			String headerText = TextTemplate.CONFIRM_ADD_RECORD.format(recordType.getText());
			FlexMessage message = FlexMessageUtils.newConfirmAndCancelMessage(headerText, recordBody);
			messageHelper.reply(replyToken, message);
		} catch (NumberFormatException e) {
			RecordMessage currentStep = RecordMessage.INPUT_AMOUNT;

			FlexMessage message = FlexMessageUtils.newMessageWithCancelFooter(currentStep.getTitle(),
					currentStep.getMessage());
			messageHelper.reply(replyToken, message);
		}
	}

	@Override
	protected Optional<ProcessHandler> getNextHandler(Member member) {
		return Optional.of(handler);
	}
}
