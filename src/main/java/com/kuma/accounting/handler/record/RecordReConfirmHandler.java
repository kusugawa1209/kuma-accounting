package com.kuma.accounting.handler.record;

import java.util.Optional;

import org.springframework.stereotype.Component;

import com.kuma.accounting.dto.TransferRecordParameter;
import com.kuma.accounting.handler.process.AbstractHandler;
import com.kuma.accounting.handler.process.ProcessHandler;
import com.kuma.accounting.message.enums.RecordType;
import com.kuma.accounting.message.enums.TextTemplate;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.utils.line.BoxUtils;
import com.kuma.accounting.utils.line.FlexMessageUtils;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;

@Component
public class RecordReConfirmHandler extends AbstractHandler {

	@Override
	protected boolean shouldHandle(Member member, String parameter) {
		return true;
	}

	@Override
	protected void tryHandle(String replyToken, Member member, String parameter) {
		TransferRecordParameter parameters = member.getParameter(TransferRecordParameter.class);

		Box recordBody;
		RecordType recordType = parameters.getType();
		if (RecordType.TRANSFER.equals(recordType)) {
			recordBody = BoxUtils.newTransferRecordInfoBody(parameters);
		} else {
			recordBody = BoxUtils.newNormalRecordInfoBody(parameters);
		}

		String headerText = TextTemplate.CONFIRM_ADD_RECORD.format(recordType.getText());
		FlexMessage message = FlexMessageUtils.newConfirmAndCancelMessage(headerText, recordBody);
		messageHelper.reply(replyToken, message);
	}

	@Override
	protected Optional<ProcessHandler> getNextHandler(Member member) {
		return Optional.empty();
	}

}
