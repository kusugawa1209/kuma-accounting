package com.kuma.accounting.handler.process;

import static com.kuma.accounting.message.enums.Message.NONE_PROCESS_SELECTED;
import static com.kuma.accounting.message.enums.Message.PROCESS_CANCELED;
import static com.kuma.accounting.message.enums.Process.CANCEL;
import static com.kuma.accounting.utils.line.BoxUtils.newFunctionMenu;
import static com.kuma.accounting.utils.line.FlexMessageUtils.newMessage;

import java.util.Optional;

import org.springframework.stereotype.Component;

import com.kuma.accounting.handler.line.AbstractMessageEventHandler;
import com.kuma.accounting.message.enums.Process;
import com.kuma.accounting.model.Member;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;

@Component
public class CancelHandler extends AbstractMessageEventHandler {

	@Override
	protected Process getSupportedProcess() {
		return CANCEL;
	}

	@Override
	protected void handleTextEvent(String replyToken, Member member, String parameter) {
		Optional<Process> processOptional = member.getProcess();
		String headerText;
		if (processOptional.isPresent()) {
			memberService.resetProcess(member);

			String processText = processOptional.get().getText();

			headerText = processText + PROCESS_CANCELED.getText();
		} else {
			headerText = NONE_PROCESS_SELECTED.getText();
		}

		Box functionMenu = newFunctionMenu();

		FlexMessage message = newMessage(headerText, functionMenu);
		messageHelper.reply(replyToken, message);
	}
}
