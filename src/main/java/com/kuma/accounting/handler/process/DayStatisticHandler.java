package com.kuma.accounting.handler.process;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.accounting.dto.DayStatistics;
import com.kuma.accounting.handler.line.AbstractMessageEventHandler;
import com.kuma.accounting.message.enums.Format;
import com.kuma.accounting.message.enums.Process;
import com.kuma.accounting.message.enums.TextTemplate;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.service.StatisticsService;
import com.kuma.accounting.utils.line.BoxUtils;
import com.kuma.accounting.utils.line.FlexMessageUtils;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;

@Component
public class DayStatisticHandler extends AbstractMessageEventHandler {

	@Autowired
	private StatisticsService statisticsService;

	@Override
	protected Process getSupportedProcess() {
		return Process.TODAY_STATISTICS;
	}

	@Override
	protected void handleTextEvent(String replyToken, Member member, String parameter) {
		DayStatistics dayStatistics = statisticsService.getDayStatistics(member, new Date());

		String headerText = getHeaderText(dayStatistics);
		Box header = BoxUtils.newHeader(headerText);

		Box body = BoxUtils.newDayStatisticBody(dayStatistics);

		Box footer = BoxUtils.newMenuFooter();

		FlexMessage message = FlexMessageUtils.newMessage(headerText, header, body, footer);
		messageHelper.reply(replyToken, message);
	}

	private String getHeaderText(DayStatistics dayStatistics) {
		DateFormat dateFormat = new SimpleDateFormat(Format.STATISTICS_DATE_FORMAT.getStringFormat());
		String dayText = dateFormat.format(dayStatistics.getDate());
		return TextTemplate.DAY_STATISTICS.format(dayText);
	}
}
