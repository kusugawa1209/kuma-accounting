package com.kuma.accounting.handler.process;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.accounting.helper.MessageHelper;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.service.AccountService;
import com.kuma.accounting.service.CreditCardService;
import com.kuma.accounting.service.MemberService;
import com.kuma.accounting.service.NormalRecordService;
import com.kuma.accounting.service.StatisticsService;
import com.kuma.accounting.service.TransferRecordService;

@Component
public abstract class AbstractHandler implements ProcessHandler {

	@Autowired
	protected MemberService memberService;

	@Autowired
	protected AccountService accountService;

	@Autowired
	protected CreditCardService creditCardService;

	@Autowired
	protected NormalRecordService normalRecordService;

	@Autowired
	protected TransferRecordService transferRecordService;

	@Autowired
	protected StatisticsService statisticsService;

	@Autowired
	protected MessageHelper messageHelper;

	protected abstract boolean shouldHandle(Member member, String parameter);

	protected abstract void tryHandle(String replyToken, Member member, String parameter);

	protected abstract Optional<ProcessHandler> getNextHandler(Member member);

	@Override
	public void handle(String replyToken, Member member, String parameter) {
		if (shouldHandle(member, parameter)) {
			tryHandle(replyToken, member, parameter);
		} else {
			Optional<ProcessHandler> nextHandlerOptional = getNextHandler(member);
			nextHandlerOptional.ifPresent(handler -> handler.handle(replyToken, member, parameter));
		}
	}
}
