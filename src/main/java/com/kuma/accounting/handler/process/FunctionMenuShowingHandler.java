package com.kuma.accounting.handler.process;

import static com.kuma.accounting.message.enums.Message.CHOOSE_FUNCTION;
import static com.kuma.accounting.message.enums.Process.SHOW_FUNCTION_MENU;
import static com.kuma.accounting.utils.line.BoxUtils.newFunctionMenu;
import static com.kuma.accounting.utils.line.FlexMessageUtils.newMessage;

import java.util.Optional;

import org.springframework.stereotype.Component;

import com.kuma.accounting.handler.line.AbstractMessageEventHandler;
import com.kuma.accounting.message.enums.Process;
import com.kuma.accounting.model.Member;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;

@Component
public class FunctionMenuShowingHandler extends AbstractMessageEventHandler {

	@Override
	protected Process getSupportedProcess() {
		return SHOW_FUNCTION_MENU;
	}

	@Override
	protected void handleTextEvent(String replyToken, Member member, String parameter) {
		Optional<Process> processOptional = member.getProcess();
		if (processOptional.isPresent()) {
			memberService.resetProcess(member);
		}

		Box functionMenu = newFunctionMenu();

		FlexMessage message = newMessage(CHOOSE_FUNCTION.getText(), functionMenu);
		messageHelper.reply(replyToken, message);
	}
}
