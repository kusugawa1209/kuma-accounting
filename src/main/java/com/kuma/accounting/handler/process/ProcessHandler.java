package com.kuma.accounting.handler.process;

import com.kuma.accounting.model.Member;

public interface ProcessHandler {

	void handle(String replyToken, Member member, String parameter);

}
