package com.kuma.accounting.handler.process;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.accounting.dto.MonthStatistics;
import com.kuma.accounting.handler.line.AbstractMessageEventHandler;
import com.kuma.accounting.message.enums.Process;
import com.kuma.accounting.message.enums.TextTemplate;
import com.kuma.accounting.model.Member;
import com.kuma.accounting.service.StatisticsService;
import com.kuma.accounting.utils.line.BoxUtils;
import com.kuma.accounting.utils.line.FlexMessageUtils;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;

@Component
public class MonthStatisticHandler extends AbstractMessageEventHandler {

	@Autowired
	private StatisticsService statisticsService;

	@Override
	protected Process getSupportedProcess() {
		return Process.MONTH_STATISTICS;
	}

	@Override
	protected void handleTextEvent(String replyToken, Member member, String parameter) {
		MonthStatistics monthStatistics = statisticsService.getMonthStatistics(member, new Date());

		String headerText = getHeaderText(monthStatistics);
		Box header = BoxUtils.newHeader(headerText);

		Box body = BoxUtils.newMonthStatisticBody(monthStatistics);

		Box footer = BoxUtils.newMenuFooter();
		
		FlexMessage message = FlexMessageUtils.newMessage(Process.MONTH_STATISTICS.getText(), header, body, footer);
		messageHelper.reply(replyToken, message);
	}

	private String getHeaderText(MonthStatistics monthStatistics) {
		return TextTemplate.MONTH_STATISTICS.format(monthStatistics.getYear(), monthStatistics.getMonth());
	}
}
