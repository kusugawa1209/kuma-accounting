package com.kuma.accounting.message.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum AccountMessage {

	ACCOUNT_NAME("請輸入帳戶名稱", "如: 中國信託活儲"),

	INITIAL_AMOUNT("請輸入帳戶餘額", "如: 50000");

	private String title;

	private String message;

}
