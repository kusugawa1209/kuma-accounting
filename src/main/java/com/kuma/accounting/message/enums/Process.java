package com.kuma.accounting.message.enums;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import lombok.Getter;

@Getter
public enum Process {

	TODAY_STATISTICS("本日統計", true),

	MONTH_STATISTICS("本月統計", true),

	ADD_ACCOUNT("新增帳戶", true),

	CHECK_ACCOUNT("查詢帳戶", true),

	EDIT_ACCOUNT("編輯帳戶", true),

	REMOVE_ACCOUNT("移除帳戶", true),

	ADD_CREDIT_CARD("新增信用卡", true),

	CHECK_CREDIT_CARD("查詢信用卡", true),

	EDIT_CREDIT_CARD("編輯信用卡", true),

	REMOVE_CREDIT_CARD("移除信用卡", true),

	ADD_RECORD("記帳", true),

	CANCEL("取消", false),

	SHOW_FUNCTION_MENU("功能選單", false);

	private String text;

	private boolean isShowInFunctionMenu;

	private Process(String text, boolean isShowInFunctionMenu) {
		this.text = text;
		this.isShowInFunctionMenu = isShowInFunctionMenu;
	}

	private List<Process> nextProcesses;

	public static Optional<Process> getTextMessageKeywordsByText(String text) {
		return Arrays.stream(Process.values()).parallel()
				.filter(keyword -> StringUtils.startsWithIgnoreCase(text, keyword.getText())).findAny();
	}

	public static List<Process> getFunctionProcesses() {
		return Arrays.stream(Process.values()).filter(Process::isShowInFunctionMenu).collect(Collectors.toList());
	}
}
