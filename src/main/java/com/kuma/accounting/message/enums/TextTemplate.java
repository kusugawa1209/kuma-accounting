package com.kuma.accounting.message.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum TextTemplate {

	RECORD_DATE_FORMAT("%s日期: %s"),

	RECORD_ACCOUNT_TYPE_FORMAT("%s類型: %s"),

	RECORD_ACCOUNT_NAME_FORMAT("%s%s名稱: %s"),

	RECORD_SOURCE_ACCOUNT_TYPE_FORMAT("轉出類型: %s"),

	RECORD_SOURCE_ACCOUNT_NAME_FORMAT("轉出%s名稱: %s"),

	RECORD_TARGET_ACCOUNT_TYPE_FORMAT("轉入類型: %s"),

	RECORD_TARGET_ACCOUNT_NAME_FORMAT("轉入%s名稱: %s"),

	RECORD_ITEM_FORMAT("%s項目: %s"),

	RECORD_AMOUNT_FORMAT("%s金額: %s"),

	ACCOUNT_NAME_FORMAT("帳戶名稱: %s"),

	CREDIT_CARD_NAME_FORMAT("信用卡名稱: %s"),
	
	BASE_ACCOUNT_NAME_INFO_FORMAT("%s資訊"),

	QUOTA_FORMAT("額度: %s"),

	CREDIT_CARD_CURRENT_AMOUNT_FORMAT("目前花費: %s"),

	ACCOUNT_CURRENT_AMOUNT_FORMAT("目前餘額: %s"),

	DUE_DATE_FORMAT("繳款截止日: 每月 %d 號"),

	BILLING_DATE_FORMAT("帳單結帳日: 每月 %d 號"),

	DATE_FORMAT("日期: %s"),

	DESCRIPTION_FORMAT("敘述: %s"),

	ACCOUNT("%s (%s)"),

	INCOME("收入: %s"),

	OUT_GOING("支出: %s"),

	SOURCE("轉出帳戶: %s"),

	TARGET("轉入帳戶: %s"),

	AMOUNT("金額: %s"),

	MONTH_RECORD_TYPE("本月%s: %s"),
	
	MONTH_INCOME_MINUS_OUT_GOING("本月收支差額: %s"),

	DAY_RECORD_TYPE("本日%s: %s"),
	
	DAY_INCOME_MINUS_OUT_GOING("本日收支差額: %s"),

	DAY_STATISTICS("%s 當日統計"),

	MONTH_STATISTICS("%02d/%02d 當月統計"),

	RECORD("%s: %s - %s"),

	TRANSFER_RECORD("%s ➜ %s: %s - %s"),

	CONFIRM_ADD_RECORD("請確認是否新增%s紀錄");

	private String format;

	public String format(Object... objects) {
		return String.format(this.format, objects);
	}
}
