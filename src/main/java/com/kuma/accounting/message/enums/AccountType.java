package com.kuma.accounting.message.enums;

import java.util.Arrays;
import java.util.Optional;

import org.apache.commons.codec.binary.StringUtils;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum AccountType {
	ACCOUNT("帳戶"), CREDIT_CARD("信用卡");

	@Getter
	private String name;

	public static Optional<AccountType> getByName(String name) {
		return Arrays.stream(AccountType.values())
				.filter(accountType -> StringUtils.equals(accountType.getName(), name)).findAny();
	}
}
