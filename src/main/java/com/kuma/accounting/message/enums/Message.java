package com.kuma.accounting.message.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum Message {

	NO_ACCOUNT_FOUND("查無帳戶"),

	NO_CREDIT_CARD_FOUND("查無信用卡"),

	NO_ACCOUNT_TITLE("你沒有任何帳戶喔..."),

	NO_CREDIT_CARD_TITLE("你沒有任何信用卡喔..."),

	ADD_ACCOUNT_OR_CREDIT_CARD_BEFORE_ADDING_RECORD("請先新增帳戶或信用卡，才能記帳"),

	NO_ACCOUNT_TEXT("快去新增帳戶吧"),

	NO_CREDIT_CARD_TEXT("快去新增信用卡吧"),

	NONE_PROCESS_SELECTED("你沒有正在進行的流程阿..."),

	CHOOSE_ACCOUNT_TEXT("請選擇帳戶或信用卡"),

	CONFIRM("確認"),

	CHECK("查詢"),

	WELCOME("歡迎使用 Kuma 記帳"),

	CHOOSE_FUNCTION("請選擇功能"),

	WRONG_FORMAT("請輸入正確格式"),

	CONFIRM_ADD_ACCOUNT("請確認是否新增帳戶"),

	CONFIRM_ADD_CREDIT_CARD("請確認是否新增信用卡"),

	CONFIRM_ADD_RECORD("請確認是否新增紀錄"),

	CONFIRM_REMOVE_ACCOUNT("請確認是否移除帳戶"),

	CONFIRM_REMOVE_CREDIT_CARD("請確認是否移除信用卡"),

	ACCOUNT_SAVED("帳戶已儲存"),

	CREDIT_CARD_SAVED("信用卡已儲存"),

	RECORD_SAVED("紀錄已儲存"),

	REMOVED("已移除"),

	ACCOUNT_REMOVED("帳戶" + REMOVED.getText()),

	CREDIT_CARD_REMOVED("信用卡" + REMOVED.getText()),

	ACCOUNT_NAME_EXISTS("帳戶名稱已存在，請重新輸入"),

	CREDIT_CARD_NAME_EXISTS("信用卡名稱已存在，請重新輸入"),

	ACCOUNT_NAME_NOT_FOUND("查無此帳戶，請重新輸入"),

	CREDIT_CARD_NAME_NOT_FOUND("查無此信用卡，請重新選擇"),

	ERROR_MESSAGE("抱歉，發生錯誤了...相關錯誤訊息已經通知管理人員了！"),

	PROCESS_CANCELED("流程已經取消囉～"),

	YESTERDAY("昨天"),

	TODAY("今天"),

	TRANSFER_INFO("轉帳資訊"),

	STATISTICS_INFO("統計資訊"),

	EMPTY_RECORDS("查無記帳紀錄");

	private String text;
}
