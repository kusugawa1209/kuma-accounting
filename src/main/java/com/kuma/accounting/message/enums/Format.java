package com.kuma.accounting.message.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum Format {

	RECORD_DATE_FORMAT("yyyyMMdd"),

	STATISTICS_DATE_FORMAT("yyyy/MM/dd"),

	STATISTICS_MONTH_FORMAT("yyyy/MM"),

	AMOUNT_FORMAT("$#,###,###");

	private String stringFormat;

}
