package com.kuma.accounting.message.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum RecordMessage {

	CHOOSE_TYPE("請選擇記帳類型", "請選擇記帳類型"),

	CHOOSE_DATE("請輸入記帳日期", "可選擇下列日期，或是輸入您想記帳的時間，如：20191225"),

	INPUT_DESCRIPTION("請輸入敘述", "如：薪水入帳、吃午餐...等"),

	CHOOSE_ACCOUNT("請選擇帳戶/信用卡", Message.CHOOSE_ACCOUNT_TEXT.getText()),

	CHOOSE_SOURCE_ACCOUNT("請選擇轉出帳戶/信用卡", Message.CHOOSE_ACCOUNT_TEXT.getText()),

	CHOOSE_TARGET_ACCOUNT("請選擇轉入帳戶/信用卡", Message.CHOOSE_ACCOUNT_TEXT.getText()),

	INPUT_AMOUNT("請輸入金額", "如：500");

	private String title;

	private String message;

}
