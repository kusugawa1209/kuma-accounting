package com.kuma.accounting.message.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum CreditCardMessage {

	CREDIT_CARD_NAME("請輸入信用卡名稱", "如: 永豐大戶卡"),

	QUOTA("請輸入額度", "請以數字輸入，如: 200000"),

	DUE_DATE("請輸入繳款截止日", "請以數字輸入，如果繳款截止日為每月15號，則輸入 15"),

	BILLING_DATE("請輸入帳單結帳日", "請以數字輸入，如果帳單結帳日為每月10號，則輸入 10"),
	
	INITIAL_AMOUNT("請輸入目前花費", "請以數字輸入，如果目前這張信用卡已經刷了5000元，則輸入 5000");

	private String title;

	private String message;

}
