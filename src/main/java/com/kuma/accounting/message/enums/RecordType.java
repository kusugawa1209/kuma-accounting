package com.kuma.accounting.message.enums;

import java.util.Arrays;
import java.util.Optional;

import org.apache.commons.codec.binary.StringUtils;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum RecordType {
	INCOME("收入", RecordHandlingType.ADDING),

	OUT_GOING("支出", RecordHandlingType.SUBSTRACTING),

	BORROW("借錢", RecordHandlingType.ADDING),

	PAY_BACK("還錢", RecordHandlingType.SUBSTRACTING),

	LEND("借出", RecordHandlingType.SUBSTRACTING),

	RECEIVE("收錢", RecordHandlingType.ADDING),

	TRANSFER("轉帳", RecordHandlingType.TRANSFERING);

	@Getter
	private String text;

	@Getter
	private RecordHandlingType handlingType;

	public static Optional<RecordType> getRecordTypeByText(String text) {
		return Arrays.stream(RecordType.values()).filter(recordType -> StringUtils.equals(recordType.getText(), text))
				.findAny();
	}
}
