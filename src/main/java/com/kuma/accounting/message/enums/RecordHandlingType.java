package com.kuma.accounting.message.enums;

public enum RecordHandlingType {
	ADDING, SUBSTRACTING, TRANSFERING;
}
