package com.kuma.accounting.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.kuma.accounting.model.CreditCard;

public interface CreditCardRepository extends JpaRepository<CreditCard, Long>, JpaSpecificationExecutor<CreditCard> {

	Optional<CreditCard> findByMemberIdAndName(String userId, String name);

	List<CreditCard> findByMemberId(String userId);

	CreditCard getByMemberIdAndId(String userId, Long creditCardId);
}
