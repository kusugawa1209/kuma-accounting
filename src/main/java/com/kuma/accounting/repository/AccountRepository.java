package com.kuma.accounting.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.kuma.accounting.model.Account;

public interface AccountRepository extends JpaRepository<Account, Long>, JpaSpecificationExecutor<Account> {

	Optional<Account> findByMemberIdAndName(String userId, String name);

	List<Account> findByMemberId(String userId);
	
	Account getByMemberIdAndId(String userId, Long accountId);

}
