package com.kuma.accounting.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.kuma.accounting.model.TransferRecord;

public interface TransferRecordRepository
extends JpaRepository<TransferRecord, Long>, JpaSpecificationExecutor<TransferRecord> {
	
	@Query("SELECT r FROM TransferRecord r "
			+ "WHERE r.member.id = ?1 "
			+ "AND YEAR(r.date) = YEAR(?2) "
			+ "AND MONTH(r.date) = MONTH(?2) "
			+ "AND DAY(r.date) = DAY(?2)")
	List<TransferRecord> findRecordsByMemberAndYearAndMonthAndDay(String userId, Date date);
}
