package com.kuma.accounting.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.kuma.accounting.model.Member;

public interface MemberRepository extends JpaRepository<Member, String>, JpaSpecificationExecutor<Member> {

}
