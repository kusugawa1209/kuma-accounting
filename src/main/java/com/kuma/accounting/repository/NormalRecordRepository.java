package com.kuma.accounting.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.kuma.accounting.model.NormalRecord;

public interface NormalRecordRepository
		extends JpaRepository<NormalRecord, Long>, JpaSpecificationExecutor<NormalRecord> {

	@Query("SELECT r FROM NormalRecord r "
			+ "WHERE r.member.id = ?1 "
			+ "AND YEAR(r.date) = YEAR(?2) "
			+ "AND MONTH(r.date) = MONTH(?2)")
	List<NormalRecord> findRecordsByMemberAndYearAndMonth(String userId, Date date);

	@Query("SELECT r FROM NormalRecord r "
			+ "WHERE r.member.id = ?1 "
			+ "AND YEAR(r.date) = YEAR(?2) "
			+ "AND MONTH(r.date) = MONTH(?2) "
			+ "AND DAY(r.date) = DAY(?2)")
	List<NormalRecord> findRecordsByMemberAndYearAndMonthAndDay(String userId, Date date);
}
