package com.kuma.accounting.model;

import static com.kuma.accounting.message.enums.AccountType.ACCOUNT;

import java.math.BigDecimal;

import javax.persistence.Entity;

import com.kuma.accounting.info.AccountInfo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Account extends BaseAccount implements AccountInfo {

	private static final long serialVersionUID = 1L;

	public Account(String name, Boolean isActive, Member member, BigDecimal initialAmount) {
		setName(name);
		setIsActive(isActive);
		setMember(member);
		setType(ACCOUNT);
		setInitialAmount(initialAmount);
		setCurrentAmount(initialAmount);
		setCount(0L);
	}

}
