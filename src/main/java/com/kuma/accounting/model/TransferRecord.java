package com.kuma.accounting.model;

import static java.util.Comparator.comparing;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.kuma.accounting.info.TransferRecordInfo;
import com.kuma.accounting.message.enums.RecordType;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class TransferRecord extends BaseRecord implements TransferRecordInfo, Comparable<TransferRecord> {

	private static final long serialVersionUID = 1L;

	@ManyToOne(optional = false)
	@JoinColumn(name = "sourceAccountId", nullable = false)
	private BaseAccount sourceAccount;

	@ManyToOne(optional = false)
	@JoinColumn(name = "targetAccountId", nullable = false)
	private BaseAccount targetAccount;

	public TransferRecord(Member member, BaseAccount sourceAccount, BaseAccount targetAccount, Date date,
			String description, BigDecimal amount) {
		setMember(member);
		setAmount(amount);
		setType(RecordType.TRANSFER);
		setDate(date);
		setDescription(description);

		this.sourceAccount = sourceAccount;
		this.targetAccount = targetAccount;
	}

	@Override
	public int compareTo(TransferRecord o) {
		return comparing(TransferRecord::getMember).thenComparing(TransferRecord::getType)
				.thenComparing(record -> record.getSourceAccount().getName())
				.thenComparing(record -> record.getTargetAccount().getName())
				.thenComparing(TransferRecord::getDescription).thenComparing(TransferRecord::getAmount)
				.thenComparing(TransferRecord::getCreatedAt).compare(this, o);
	}
}
