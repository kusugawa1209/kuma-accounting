package com.kuma.accounting.model;

import static java.util.Comparator.comparing;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.kuma.accounting.info.NormalRecordInfo;
import com.kuma.accounting.message.enums.RecordType;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class NormalRecord extends BaseRecord implements NormalRecordInfo, Comparable<NormalRecord> {

	private static final long serialVersionUID = 1L;

	@ManyToOne(optional = false)
	@JoinColumn(name = "accountId", nullable = false)
	private BaseAccount account;

	public NormalRecord(Member member, BaseAccount account, RecordType type, Date date, String description,
			BigDecimal amount) {
		setMember(member);
		setAmount(amount);
		setType(type);
		setDate(date);
		setDescription(description);

		this.account = account;
	}

	@Override
	public int compareTo(NormalRecord o) {
		return comparing(NormalRecord::getMember).thenComparing(NormalRecord::getType)
				.thenComparing(record -> record.getAccount().getName()).thenComparing(NormalRecord::getDescription)
				.thenComparing(NormalRecord::getAmount).thenComparing(NormalRecord::getCreatedAt).compare(this, o);
	}
}
