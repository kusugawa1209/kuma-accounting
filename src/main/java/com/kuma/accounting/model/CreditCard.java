package com.kuma.accounting.model;

import static com.kuma.accounting.message.enums.AccountType.CREDIT_CARD;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.kuma.accounting.info.CreditCardInfo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class CreditCard extends BaseAccount implements CreditCardInfo {

	private static final long serialVersionUID = 1L;

	@Column(nullable = false)
	private BigDecimal quota;

	@Column(nullable = false)
	private Integer dueDate;

	@Column(nullable = false)
	private Integer billingDate;

	public CreditCard(String name, Boolean isActive, BigDecimal quota, Integer dueDate, Integer billingDate,
			BigDecimal initialAmount, Member member) {
		setName(name);
		setIsActive(isActive);
		setMember(member);
		setType(CREDIT_CARD);
		setInitialAmount(initialAmount);
		setCurrentAmount(initialAmount);
		setCount(0L);

		this.quota = quota;
		this.dueDate = dueDate;
		this.billingDate = billingDate;
	}
}
