package com.kuma.accounting.model;

import static java.util.Comparator.comparing;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.kuma.accounting.info.BaseAccountInfo;
import com.kuma.accounting.message.enums.AccountType;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@EqualsAndHashCode(of = "id", callSuper = false)
public abstract class BaseAccount extends AuditDatetime implements Comparable<BaseAccount>, BaseAccountInfo {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	private Boolean isActive;

	@Column
	@Enumerated(EnumType.STRING)
	private AccountType type;

	@Column
	private BigDecimal initialAmount;

	@Column
	private BigDecimal currentAmount;

	@Column
	private Long count;

	@ManyToOne(optional = false)
	@JoinColumn(name = "memberId", nullable = false)
	private Member member;

	@Getter(value = AccessLevel.PRIVATE)
	@OneToMany(mappedBy = "account", fetch = FetchType.LAZY, orphanRemoval = true)
	private List<NormalRecord> incomeRecords;

	@Override
	public int compareTo(BaseAccount o) {
		return comparing(BaseAccount::getMember).thenComparing(BaseAccount::getCount, Collections.reverseOrder())
				.thenComparing(BaseAccount::getName).thenComparing(BaseAccount::getCreatedAt).compare(this, o);
	}

}
