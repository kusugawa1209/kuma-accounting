package com.kuma.accounting.model;

import static java.util.Comparator.comparing;
import static java.util.Objects.nonNull;
import static java.util.Optional.empty;
import static java.util.Optional.of;

import java.util.List;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.kuma.accounting.dto.ProcessParameter;
import com.kuma.accounting.message.enums.Process;
import com.kuma.utils.JsonUtils;
import com.kuma.utils.LogUtils;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode(of = "id", callSuper = false)
public class Member extends AuditDatetime implements Comparable<Member> {

	public Member(String id, String username, boolean isActive) {
		this.id = id;
		this.name = username;
		this.isActive = isActive;
	}

	private static final long serialVersionUID = 1L;

	@Id
	@NonNull
	private String id;

	@NonNull
	@Column(nullable = false)
	private String name;

	@NonNull
	@Column(nullable = false)
	private Boolean isActive;

	@Column
	@Getter(value = AccessLevel.NONE)
	@Enumerated(EnumType.STRING)
	private Process process;

	@Column
	@Getter(value = AccessLevel.NONE)
	private String parameters;

	@Getter(value = AccessLevel.PRIVATE)
	@OneToMany(mappedBy = "member", fetch = FetchType.LAZY, orphanRemoval = true)
	private List<BaseAccount> baseAccounts;

	@Getter(value = AccessLevel.PRIVATE)
	@OneToMany(mappedBy = "member", fetch = FetchType.LAZY, orphanRemoval = true)
	private List<BaseRecord> baseRecords;

	public Optional<Process> getProcess() {
		if (nonNull(this.process)) {
			return of(this.process);
		} else {
			return empty();
		}
	}

	public Optional<String> getParameters() {
		if (nonNull(this.parameters)) {
			return of(this.parameters);
		} else {
			return empty();
		}
	}

	public <T> T getParameter(Class<T> clazz) {
		Optional<String> parametersOptional = this.getParameters();
		if (parametersOptional.isPresent()) {
			return JsonUtils.toObject(parametersOptional.get(), clazz);
		} else {
			try {
				return clazz.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				LogUtils.logThrowable(e);

				return null;
			}
		}
	}

	public void setParameter(ProcessParameter processParameter) {
		this.parameters = JsonUtils.toJsonString(processParameter);
	}

	@Override
	public int compareTo(Member o) {
		return comparing(Member::getName).thenComparing(Member::getCreatedAt).thenComparing(Member::getUpdatedAt)
				.compare(this, o);
	}
}
